
package com.votermanagement.superadmin.model.AddDepartment;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("dept_id")
    private Long mDeptId;
    @SerializedName("dept_name")
    private String mDeptName;

    public Long getDeptId() {
        return mDeptId;
    }

    public void setDeptId(Long deptId) {
        mDeptId = deptId;
    }

    public String getDeptName() {
        return mDeptName;
    }

    public void setDeptName(String deptName) {
        mDeptName = deptName;
    }

}
