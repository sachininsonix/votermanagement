
package com.votermanagement.superadmin.model.addvolunteer;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("area")
    private String mArea;
    @SerializedName("created_date")
    private String mCreatedDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("name")
    private String mName;
    @SerializedName("pardhan_id")
    private Long mPardhanId;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("psw")
    private String mPsw;
    @SerializedName("role")
    private String mRole;
    @SerializedName("username")
    private String mUsername;

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long getPardhanId() {
        return mPardhanId;
    }

    public void setPardhanId(Long pardhanId) {
        mPardhanId = pardhanId;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPsw() {
        return mPsw;
    }

    public void setPsw(String psw) {
        mPsw = psw;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
