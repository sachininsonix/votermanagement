
package com.votermanagement.superadmin.model.AddResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("area_id")
    private Long mAreaId;
    @SerializedName("area_name")
    private String mAreaName;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("lng")
    private String mLng;

    public Long getAreaId() {
        return mAreaId;
    }

    public void setAreaId(Long areaId) {
        mAreaId = areaId;
    }

    public String getAreaName() {
        return mAreaName;
    }

    public void setAreaName(String areaName) {
        mAreaName = areaName;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String lat) {
        mLat = lat;
    }

    public String getLng() {
        return mLng;
    }

    public void setLng(String lng) {
        mLng = lng;
    }

}
