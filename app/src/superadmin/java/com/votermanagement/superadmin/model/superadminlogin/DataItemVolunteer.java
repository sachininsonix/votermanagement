package com.votermanagement.superadmin.model.superadminlogin;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class DataItemVolunteer implements Serializable {

	@SerializedName("area_name")
	private Object areaName;

	@SerializedName("filename")
	private String filename;

	@SerializedName("attachment")
	private String attachment;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("complaint_date")
	private String complaintDate;

	@SerializedName("department_name")
	private String departmentName;

	@SerializedName("complaint_title")
	private String complaintTitle;

	@SerializedName("details")
	private String details;

	@SerializedName("id")
	private String id;

	@SerializedName("status")
	private String status;

	@SerializedName("username")
	private String username;

	public void setAreaName(Object areaName){
		this.areaName = areaName;
	}

	public Object getAreaName(){
		return areaName;
	}

	public void setFilename(String filename){
		this.filename = filename;
	}

	public String getFilename(){
		return filename;
	}

	public void setAttachment(String attachment){
		this.attachment = attachment;
	}

	public String getAttachment(){
		return attachment;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setComplaintDate(String complaintDate){
		this.complaintDate = complaintDate;
	}

	public String getComplaintDate(){
		return complaintDate;
	}

	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}

	public String getDepartmentName(){
		return departmentName;
	}

	public void setComplaintTitle(String complaintTitle){
		this.complaintTitle = complaintTitle;
	}

	public String getComplaintTitle(){
		return complaintTitle;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"area_name = '" + areaName + '\'' + 
			",filename = '" + filename + '\'' + 
			",attachment = '" + attachment + '\'' + 
			",user_id = '" + userId + '\'' + 
			",complaint_date = '" + complaintDate + '\'' + 
			",department_name = '" + departmentName + '\'' + 
			",complaint_title = '" + complaintTitle + '\'' + 
			",details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}