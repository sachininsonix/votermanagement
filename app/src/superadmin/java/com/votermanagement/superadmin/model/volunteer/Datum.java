
package com.votermanagement.superadmin.model.volunteer;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("area")
    private String mArea;
    @SerializedName("created_date")
    private String mCreatedDate;
    @SerializedName("current_lat")
    private String mCurrentLat;
    @SerializedName("current_long")
    private String mCurrentLong;
    @SerializedName("device_id")
    private String mDeviceId;
    @SerializedName("device_token")
    private String mDeviceToken;
    @SerializedName("device_type")
    private String mDeviceType;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("psw")
    private String mPsw;
    @SerializedName("role")
    private String mRole;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("updated_date")
    private String mUpdatedDate;
    @SerializedName("username")
    private String mUsername;

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        mCreatedDate = createdDate;
    }

    public String getCurrentLat() {
        return mCurrentLat;
    }

    public void setCurrentLat(String currentLat) {
        mCurrentLat = currentLat;
    }

    public String getCurrentLong() {
        return mCurrentLong;
    }

    public void setCurrentLong(String currentLong) {
        mCurrentLong = currentLong;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String deviceType) {
        mDeviceType = deviceType;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPsw() {
        return mPsw;
    }

    public void setPsw(String psw) {
        mPsw = psw;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        mUpdatedDate = updatedDate;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
