
package com.votermanagement.superadmin.model.areResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Datum {

    @SerializedName("area_name")
    private String mAreaName;
    @SerializedName("assigned_pradhan_id")
    private String mAssignedPradhanId;
    @SerializedName("id")
    private String mId;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("lng")
    private String mLng;

    public String getAreaName() {
        return mAreaName;
    }

    public void setAreaName(String areaName) {
        mAreaName = areaName;
    }

    public String getAssignedPradhanId() {
        return mAssignedPradhanId;
    }

    public void setAssignedPradhanId(String assignedPradhanId) {
        mAssignedPradhanId = assignedPradhanId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String lat) {
        mLat = lat;
    }

    public String getLng() {
        return mLng;
    }

    public void setLng(String lng) {
        mLng = lng;
    }

}
