package com.votermanagement.superadmin.presenter;

import android.content.Context;
import android.util.Log;

import com.votermanagement.main.activity.network.ApiClient;
import com.votermanagement.superadmin.model.generic.GenericResponse;
import com.votermanagement.superadmin.network.ApiInterface;
import com.votermanagement.superadmin.utils.CommonUtils;
import com.votermanagement.superadmin.view.fragment.SendView;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.votermanagement.superadmin.utils.CommonUtils.showProgressDialog;

/**
 * Created by insonix on 13/9/17.
 */

public class SendNotificationPresenterImpl implements SendNotificationPresenter {
    Context mCtx;
    SendView mView;
    private ApiInterface mApi;

    public SendNotificationPresenterImpl(Context ctt, SendView view) {
        this.mCtx=ctt;
        this.mView=view;
    }

    @Override
    public void sendNotification(String id, String title, String msg, String type, File imgFile, String name, String notiType) {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
        showProgressDialog(mCtx);
        RequestBody frmid = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody Title = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody Msg = RequestBody.create(MediaType.parse("text/plain"), msg);
        RequestBody Type = RequestBody.create(MediaType.parse("text/plain"), type);
        RequestBody imgName = RequestBody.create(MediaType.parse("text/plain"), name);
//        RequestBody img = RequestBody.create(MediaType.parse("image/*"), imgFile);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", imgFile.getName(), RequestBody.create(MediaType.parse("image/*"), imgFile));
        RequestBody NotiType = RequestBody.create(MediaType.parse("text/plain"), notiType);
        mApi.sendNotification(frmid,Title,Msg,Type,filePart,imgName,NotiType).enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                CommonUtils.dismissDialog();
                mView.SuccessFul(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                CommonUtils.dismissDialog();
                Log.i(TAG, "onFailure: "+t.getMessage());
                mView.SuccessFul("error"+t.getMessage());
            }
        });
    }
}
