package com.votermanagement.superadmin.presenter;

import android.content.Context;

import com.votermanagement.main.activity.model.AreaResponse;
import com.votermanagement.main.activity.network.ApiClient;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;
import com.votermanagement.superadmin.network.ApiInterface;
import com.votermanagement.superadmin.utils.CommonUtils;
import com.votermanagement.superadmin.view.fragment.VolunteerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.votermanagement.superadmin.utils.CommonUtils.*;

/**
 * Created by insonix on 11/9/17.
 */

public class VolunteerPresenterImpl implements VolunteerPresenter {

    VolunteerView mView;
    ApiInterface mApi;
    Context mCtx;

    public VolunteerPresenterImpl(Context ctt,VolunteerView view) {
        mView=view;
        mCtx=ctt;
    }

    @Override
    public void getVolunteerList(String areaId) {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
//        showProgressDialog(mCtx);
        mApi.showVolunteerList(areaId).enqueue(new Callback<VolunteerList>() {
            @Override
            public void onResponse(Call<VolunteerList> call, Response<VolunteerList> response) {
//                dismissDialog();
                mView.onListLoaded(response);
            }

            @Override
            public void onFailure(Call<VolunteerList> call, Throwable t) {
//                dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getAreaList() {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
        showProgressDialog(mCtx);
        mApi.getAreaList().enqueue(new Callback<AreaList>() {
            @Override
            public void onResponse(Call<AreaList> call, Response<AreaList> response) {
                CommonUtils.dismissDialog();
                mView.showAreasList(response);
            }

            @Override
            public void onFailure(Call<AreaList> call, Throwable t) {
                CommonUtils.dismissDialog();
                mView.onError(t.getMessage());

            }
        });
    }

    @Override
    public void addVolunteer(String name, String username, String email, String password, String phone, String area) {
        CommonUtils.showProgressDialog(mCtx);
        mApi.addVolunteer(name,username,email,password,phone,area,"1").enqueue(new Callback<AreaResponse>() {
            @Override
            public void onResponse(Call<AreaResponse> call, Response<AreaResponse> response) {
                CommonUtils.dismissDialog();
                mView.onError(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<AreaResponse> call, Throwable t) {
                CommonUtils.dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }

}
