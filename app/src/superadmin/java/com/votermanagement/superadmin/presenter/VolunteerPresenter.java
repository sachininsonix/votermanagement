package com.votermanagement.superadmin.presenter;

/**
 * Created by insonix on 11/9/17.
 */

public interface VolunteerPresenter{

    void getVolunteerList(String areaId);

    void getAreaList();

    void addVolunteer(String name,String username,String email,String password,String phone,String area);
}
