package com.votermanagement.superadmin.presenter;

/**
 * Created by insonix on 12/9/17.
 */

public interface AddAreaDept {

    void addArea(String areaName,String latlng);

    void addDeptt(String depttName);

    void getDepttList();

    void getAreasList();
}
