package com.votermanagement.superadmin.presenter;

import java.io.File;

import okhttp3.MultipartBody;

/**
 * Created by insonix on 13/9/17.
 */

public interface SendNotificationPresenter {


    void sendNotification(String id, String title, String msg, String type, File file, String Filename, String notiType);


}
