package com.votermanagement.superadmin.presenter;

import android.content.Context;

import com.votermanagement.main.activity.network.ApiClient;
import com.votermanagement.superadmin.model.AddDepartment.AddDepttResponse;
import com.votermanagement.superadmin.model.AddResponse.AddAreaResponse;
import com.votermanagement.superadmin.model.DepttResponse.DepttListResponse;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.network.ApiInterface;
import com.votermanagement.superadmin.view.fragment.AddView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.votermanagement.superadmin.utils.CommonUtils.dismissDialog;
import static com.votermanagement.superadmin.utils.CommonUtils.showProgressDialog;

/**
 * Created by insonix on 12/9/17.
 */

public class AddAreaDeptImpl implements AddAreaDept {


    ApiInterface mApi;
    Context mCtx;
    AddView mView;

    public AddAreaDeptImpl(Context ctt, AddView view) {
        mView=view;
        mCtx=ctt;
    }


    @Override
    public void addArea(String areaName,String latlng) {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
//        showProgressDialog(mCtx);
        mApi.addArea(areaName,latlng.split(",")[0],latlng.split(",")[1]).enqueue(new Callback<AddAreaResponse>() {
            @Override
            public void onResponse(Call<AddAreaResponse> call, Response<AddAreaResponse> response) {
//                dismissDialog();
                mView.onSuccessAdd(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<AddAreaResponse> call, Throwable t) {
//                dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }

    @Override
    public void addDeptt(String name) {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
        showProgressDialog(mCtx);
        mApi.addDeptt(name).enqueue(new Callback<AddDepttResponse>() {
            @Override
            public void onResponse(Call<AddDepttResponse> call, Response<AddDepttResponse> response) {
                dismissDialog();
                mView.onSuccessAdd(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<AddDepttResponse> call, Throwable t) {
                dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getAreasList() {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
//        showProgressDialog(mCtx);
        mApi.getAreaList().enqueue(new Callback<AreaList>() {
            @Override
            public void onResponse(Call<AreaList> call, Response<AreaList> response) {
//                dismissDialog();
                mView.onListLoaded(response);
            }

            @Override
            public void onFailure(Call<AreaList> call, Throwable t) {
//                dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getDepttList() {
        mApi = ApiClient.getInstance().create(ApiInterface.class);
//        showProgressDialog(mCtx);
        mApi.getDepttList().enqueue(new Callback<DepttListResponse>() {
            @Override
            public void onResponse(Call<DepttListResponse> call, Response<DepttListResponse> response) {
//                dismissDialog();
                mView.onDepttListLoaded(response.body().getData());
            }

            @Override
            public void onFailure(Call<DepttListResponse> call, Throwable t) {
//                dismissDialog();
                mView.onError(t.getMessage());
            }
        });
    }
}
