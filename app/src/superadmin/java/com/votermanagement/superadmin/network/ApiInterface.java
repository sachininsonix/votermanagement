package com.votermanagement.superadmin.network;


import com.votermanagement.main.activity.model.AddComplaintResponse;
import com.votermanagement.main.activity.model.AreaResponse;
import com.votermanagement.main.activity.model.Comment;
import com.votermanagement.main.activity.model.DepartmentResponse;
import com.votermanagement.main.activity.model.ListNotificationResponse;
import com.votermanagement.main.activity.model.LoginResponse;
import com.votermanagement.main.activity.model.Reply;
import com.votermanagement.main.activity.model.SignupResponse;
import com.votermanagement.main.activity.model.SingleReplyResponse;
import com.votermanagement.main.activity.model.UserComplaintListResponse;
import com.votermanagement.superadmin.model.AddDepartment.AddDepttResponse;
import com.votermanagement.superadmin.model.AddResponse.AddAreaResponse;
import com.votermanagement.superadmin.model.DepttResponse.DepttListResponse;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.generic.GenericResponse;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Sandeep on 8/3/2017.
 */

public interface ApiInterface {

    @GET("userlogin")
    Call<LoginResponse> login(@Query("username") String userName, @Query("password") String pwd, @Query("device_id") String device_id, @Query("device_token") String device_token, @Query("device_type") String device_type);

    @GET("signUp")
    Call<SignupResponse> signUp(@Query("name") String userName, @Query("psw") String psw, @Query("phone") String phone, @Query("device_id") String device_id, @Query("device_token") String device_token, @Query("device_type") String device_type, @Query("area") String area, @Query("lat") String lat, @Query("long") String lon);

    @Multipart
    @POST("addComplaint")
    Call<AddComplaintResponse> addComplaint(@Part("complaint_title") RequestBody complaintTitle, @Part("department_id") RequestBody departmentId, @Part MultipartBody.Part part, @Part("user_id") RequestBody userId, @Part("details") RequestBody details, @Part("filename") RequestBody filename);

    @GET("getUsersComplaint")
    Call<UserComplaintListResponse> showComplaintListAndStatus(@Query("user_id") String user_id);


    @GET("getComplaintsByStatus")
    Call<UserComplaintListResponse> getComplaintsVolunteer(@Query("area_id") String area_id);

    @GET("getAreas")
    Call<AreaResponse> showAreaList();

    @GET("getAllDepartments")
    Call<DepartmentResponse> showDepartmentList();

    @GET("addReplyToComplaint")
    Call<Comment> postComment(@Query("complaint_id") String complaint_id, @Query("message") String message, @Query("user_id") String user_id, @Query("status") String status);

    @GET("getAllReply?")
    Call<Reply> getCommentList(@Query("complaint_id") String complaint_id);

    @GET("getReplyDetail")
    Call<SingleReplyResponse> getComment(@Query("reply_id") String reply_id);

    @GET("getUserNotifications")
    Call<ListNotificationResponse> getNotificationList(@Query("user_id") String user_id);

    @GET("VolunteerList")
    Call<VolunteerList> showVolunteerList(@Query("area_id") String area_id);

    @GET("add_user")
    Call<AreaResponse> addVolunteer(@Query("name") String name, @Query("username") String userName, @Query("email") String email, @Query("password") String password, @Query("phone") String phone, @Query("area_id") String area_id,
                                    @Query("role")
                                            String role);

    @GET("getAreas")
    Call<AreaList> getAreaList();

    @GET("add_area")
    Call<AddAreaResponse> addArea(@Query("area_name") String areaName, @Query("lat") String lat,@Query("lng") String lng);

    @GET("getAllDepartments")
    Call<DepttListResponse> getDepttList();

    @GET("add_dept")
    Call<AddDepttResponse> addDeptt(@Query("dept_name") String name);

    @Multipart
    @POST("send_notification")
    Call<GenericResponse> sendNotification(@Part("from_id")RequestBody fromId, @Part("title")RequestBody title,
                                           @Part("message")RequestBody msg, @Part("user_role")RequestBody role,
                                           @Part MultipartBody.Part file,
                                           @Part("image_name")RequestBody name, @Part("not_type")RequestBody Notitype);

}