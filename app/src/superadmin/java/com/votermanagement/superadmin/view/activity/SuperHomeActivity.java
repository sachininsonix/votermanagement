package com.votermanagement.superadmin.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.votermanagement.main.R;
import com.votermanagement.main.activity.view.HomeActivity;
import com.votermanagement.main.activity.view.LoginActivity;
import com.votermanagement.superadmin.view.fragment.AddAreaFragment;
import com.votermanagement.superadmin.view.fragment.AreaFragment;
import com.votermanagement.superadmin.view.fragment.ComplaintListFragment;
import com.votermanagement.superadmin.view.fragment.DepttFragment;
import com.votermanagement.superadmin.view.fragment.HomeFragment;
import com.votermanagement.superadmin.view.fragment.SendNotificationFragment;
import com.votermanagement.superadmin.view.fragment.VolunteerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Sandeep on 8/9/2017.
 */

public class SuperHomeActivity extends AppCompatActivity {
    public static final String TAG = HomeActivity.class.getSimpleName();
    @BindView(R.id.navigationView)
    NavigationView mNavigationView;
    @BindView(R.id.drawer)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.app_layout)
    AppBarLayout appLayout;
    @BindView(R.id.container)
    FrameLayout container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superhome);
        ButterKnife.bind(this);
        initNavigationView();
        mToolbar.getOverflowIcon();
        mToolbar.setTitle("KKSA");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setElevation(0);
        }
        getSupportFragmentManager().beginTransaction().add(R.id.container, new HomeFragment()).addToBackStack("list").commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            this.finish();
        }
    }

    private void initNavigationView() {
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_super:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).addToBackStack("list").commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.complaints:
                        getSupportFragmentManager().beginTransaction().add(R.id.container, new ComplaintListFragment()).addToBackStack("complaints").commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.volunteer:
                        getSupportFragmentManager().beginTransaction().add(R.id.container, new VolunteerFragment()).addToBackStack("list").commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.area:
                        getSupportFragmentManager().beginTransaction().add(R.id.container, new AreaFragment()).addToBackStack("area").commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.deptt:
                        getSupportFragmentManager().beginTransaction().add(R.id.container, new DepttFragment()).addToBackStack("dept").commit();
                        mDrawerLayout.closeDrawers();
                        break;
                    case R.id.sendNoti:
                        getSupportFragmentManager().beginTransaction().add(R.id.container, new SendNotificationFragment()).addToBackStack("noti").commit();
                        mDrawerLayout.closeDrawers();
                        break;

                    case R.id.logout:
                        new AlertDialog.Builder(SuperHomeActivity.this)
                                .setTitle("Closing Activity")
                                .setMessage("Are you sure you want to logout?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SharedPreferences sharedPreferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                                        sharedPreferences.edit().remove("loginStatus").commit();
                                        Intent intent = new Intent(SuperHomeActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                })
                                .show();

                        mDrawerLayout.closeDrawers();
                        break;
                }
                return true;
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        View header = mNavigationView.getHeaderView(0);
        TextView textView = header.findViewById(R.id.name);
        TextView textView1 = header.findViewById(R.id.email);
        textView.setText("SuperAdmin");
        textView.setTextColor(Color.WHITE);
        textView1.setTextColor(Color.WHITE);

    }

}
