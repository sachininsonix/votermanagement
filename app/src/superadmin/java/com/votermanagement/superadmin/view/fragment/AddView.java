package com.votermanagement.superadmin.view.fragment;

import com.votermanagement.main.activity.model.AreaResponse;
import com.votermanagement.superadmin.model.DepttResponse.Datum;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;

import java.util.List;

import retrofit2.Response;

/**
 * Created by insonix on 12/9/17.
 */

public interface AddView {

    void onSuccessAdd(String message);

    void onError(String message);


    void onListLoaded(Response<AreaList> response);

    void onDepttListLoaded(List<Datum> data);
}
