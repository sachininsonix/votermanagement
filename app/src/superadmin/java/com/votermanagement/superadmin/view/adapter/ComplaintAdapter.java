package com.votermanagement.superadmin.view.adapter;

/**
 * Created by insonix on 15/9/17.
 */

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.votermanagement.main.R;
import com.votermanagement.main.activity.interfaces.Icontants;
import com.votermanagement.main.activity.model.UserComplaintListData;
import com.votermanagement.main.activity.model.UserComplaintListResponse;
import com.votermanagement.main.activity.view.IdeaDetailActivity;
import com.votermanagement.superadmin.utils.CircleTransform;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

//final LoginResponse loginResponse = gson.fromJson(json, LoginResponse.class);

/**
 * Created by root on 30/1/17.
 */
public class ComplaintAdapter extends RecyclerView.Adapter<ComplaintAdapter.ItemViewHolder> implements Icontants {

    private String BASE_URL = "http://khersocialapp.insonix.com/";
    private FragmentActivity mContext;
    String data;
    private List<UserComplaintListData> idealist;
    //private List<DataItemVolunteer> volnterrData;
    int len;
    String role;

    String finalStatus;

    public ComplaintAdapter(FragmentActivity activity, String data, String role) {
        mContext = activity;
        this.role = role;
        this.data = data;
        Gson gson = new Gson();

        if (role.equals(USER)) {
            UserComplaintListResponse userComplaintListData = gson.fromJson(data, UserComplaintListResponse.class);
            idealist = userComplaintListData.getData();
            len = idealist.size();

        } else if (role.equals(VOULNTEER)) {
            UserComplaintListResponse userComplaintListData = gson.fromJson(data, UserComplaintListResponse.class);
            idealist = userComplaintListData.getData();
            len = idealist.size();
        } else {
            UserComplaintListResponse userComplaintListData = gson.fromJson(data, UserComplaintListResponse.class);
            idealist = userComplaintListData.getData();
            len = idealist.size();
        }
    }


    @Override
    public ComplaintAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new View
        View v = LayoutInflater.from(mContext).inflate(R.layout.view_idea_list_item, parent, false);
        return new ComplaintAdapter.ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ComplaintAdapter.ItemViewHolder holder, final int position) {
            final UserComplaintListData model = idealist.get(position);
            bind(model, holder,position);
    }


    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCount: " + len);
        return len;
    }

    public void setFilter(ArrayList<UserComplaintListData> idealist1) {
        Log.e("listdata", String.valueOf(idealist1));
        idealist = new ArrayList<>();
        idealist.addAll(idealist1);
        notifyDataSetChanged();
    }


    public void bind(final UserComplaintListData ideaModel, ItemViewHolder holder, int position) {
        if (!TextUtils.isEmpty(ideaModel.getAttachment())) {
            Picasso.with(mContext).load(ideaModel.getAttachment()).fit().transform(new CircleTransform()).placeholder(R.drawable.img_icon).error(R.drawable.img_icon).into(holder.ideaImage);
        }
        holder.title.setText(ideaModel.getComplaintTitle());
        holder.desc.setText(ideaModel.getDetails());

        if (ideaModel.getComplaintStatus().equals("0")) {
            finalStatus = "Pending";
        } else if (ideaModel.getComplaintStatus().equals("1")) {
            finalStatus = "In Progress";
        } else if (ideaModel.getComplaintStatus().equals("2")) {
            finalStatus = "Declined";
        } else {
            finalStatus = "Resolved";
        }
        Log.i(TAG, "bind: "+finalStatus);
        holder.status.setText(finalStatus);
        String filenameArray[] = ideaModel.getAttachment().split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        Log.d(TAG, "bind: " + extension);
        if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg")) {
            holder.imageAttachment.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(extension) && !ideaModel.getAttachment().equals(BASE_URL + "assets/flat_images/")) {
            holder.imageAttachment.setVisibility(View.VISIBLE);
        }
        Log.d(TAG, "extension: " + ideaModel.getAttachment());

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(mContext, IdeaDetailActivity.class);
                intent.putExtra("details",ideaModel);
                mContext.startActivity(intent);

            }
        });

    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.idea_image)
        ImageView ideaImage;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.image_attachment)
        ImageView imageAttachment;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;


        public ItemViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
        }
    }
}


