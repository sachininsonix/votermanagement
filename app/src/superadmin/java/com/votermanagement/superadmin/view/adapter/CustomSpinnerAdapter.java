package com.votermanagement.superadmin.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.areResponse.Datum;

import java.util.List;

/**
 * Created by insonix on 11/9/17.
 */

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<Datum> mAreas;

    public CustomSpinnerAdapter(Context applicationContext,List<Datum> areaNames) {
        this.context = applicationContext;
        this.mAreas = areaNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return mAreas.size();
    }

    @Override
    public Object getItem(int i) {
        return mAreas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = view.findViewById(R.id.spinner_item_text);
        names.setText(mAreas.get(i).getAreaName());
        return view;
    }
}
