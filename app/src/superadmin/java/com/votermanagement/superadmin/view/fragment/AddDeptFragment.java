package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.DepttResponse.Datum;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.presenter.AddAreaDept;
import com.votermanagement.superadmin.presenter.AddAreaDeptImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by insonix on 12/9/17.
 */

public class AddDeptFragment extends Fragment implements AddView {

    public static final String TAG = AddAreaFragment.class.getName();
    @BindView(R.id.deptt)
    EditText deptt;
    @BindView(R.id.add_dept_bttn)
    Button addDeptBttn;
    Unbinder unbinder;
    AddAreaDept mPresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_dept, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter = new AddAreaDeptImpl(getActivity(), this);
        addDeptBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate())
                mPresenter.addDeptt(deptt.getText().toString().trim());
            }
        });
        return view;
    }

    private boolean validate() {
        if(deptt.getText().toString().isEmpty()){
            Snackbar.make(getView(), "Department can'tbe blank", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessAdd(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container, new DepttFragment()).commit();
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onListLoaded(Response<AreaList> response) {

    }

    @Override
    public void onDepttListLoaded(List<Datum> data) {

    }

    @OnClick(R.id.add_dept_bttn)
    public void onViewClicked() {
    }
}
