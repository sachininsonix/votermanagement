package com.votermanagement.superadmin.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.votermanagement.main.R;
import com.votermanagement.main.activity.adapter.IdeasListAdapter;
import com.votermanagement.main.activity.model.LoginResponse;
import com.votermanagement.main.activity.model.UserComplaintListResponse;
import com.votermanagement.main.activity.network.ApiClient;
import com.votermanagement.main.activity.request_interface.ApiInterface;
import com.votermanagement.superadmin.view.adapter.ComplaintAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.votermanagement.superadmin.utils.CommonUtils.dismissDialog;
import static com.votermanagement.superadmin.utils.CommonUtils.showProgressDialog;

/**
 * Created by insonix on 14/9/17.
 */

public class ComplaintListFragment extends Fragment {

    private static final String TAG = ComplaintListFragment.class.getName();
    @BindView(R.id.mRecycleIdeaList)
    RecyclerView mRecycleIdeaList;
    @BindView(R.id.nocomment)
    TextView nocomment;
    @BindView(R.id.mainLayout)
    CoordinatorLayout mainLayout;
    Unbinder unbinder;
    @BindView(R.id.loader)
    ProgressBar loader;
    private ComplaintAdapter adapter;
    private GridLayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.complaint_list_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        onComplaintList();
        return view;
    }

    public void onComplaintList() {
        showProgressDialog(getActivity());
        SharedPreferences sharedPref = getActivity().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String userId = sharedPref.getString("userId", "");
        Gson gson = new Gson();
        String json = sharedPref.getString("loginResponse", "");
        final LoginResponse loginResponse = gson.fromJson(json, LoginResponse.class);
        ApiInterface apiInterface = ApiClient.getInstance().create(ApiInterface.class);
        Call<UserComplaintListResponse> result = apiInterface.showComplaintListAndStatus("2");
        result.enqueue(new Callback<UserComplaintListResponse>() {
            @Override
            public void onResponse(Call<UserComplaintListResponse> call, Response<UserComplaintListResponse> response) {

                Log.i(TAG, "onResponse: " + response.body().getData());
                if(response.body().getMessage().equalsIgnoreCase("compalint list")){
                    Gson gson = new Gson();
                    String data = gson.toJson(response.body());
                    adapter = new ComplaintAdapter(getActivity(), data, loginResponse.getRole());
                    mLayoutManager = new GridLayoutManager(getActivity(), 1);
                    mRecycleIdeaList.setLayoutManager(mLayoutManager);
                    mRecycleIdeaList.setAdapter(adapter);
                }else if(response.body().getMessage().equalsIgnoreCase("No complaint found")){
                    mRecycleIdeaList.setVisibility(View.GONE);
                    nocomment.setVisibility(View.VISIBLE);
                    nocomment.setText("No complaints found");
                }
                dismissDialog();
                //   setDataUser(response.body());
            }

            @Override
            public void onFailure(Call<UserComplaintListResponse> call, Throwable t) {
                Log.i("Tag", "onFailure: " + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
