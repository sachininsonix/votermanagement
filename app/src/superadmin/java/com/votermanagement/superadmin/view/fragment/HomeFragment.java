package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.view.adapter.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 13/9/17.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.tabs)
    TabLayout tabs;
    Unbinder unbinder;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Log.i(TAG, "onCreateView: Home super");
        unbinder = ButterKnife.bind(this, view);
        tabs.addTab(tabs.newTab().setText("Map"));
        tabs.addTab(tabs.newTab().setText("Complaints"));
        setupViewPager(viewpager);
        tabs.setupWithViewPager(viewpager);
        return view;
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new MapFragment(), "Map");
        adapter.addFragment(new ComplaintListFragment(), "Complaints");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
