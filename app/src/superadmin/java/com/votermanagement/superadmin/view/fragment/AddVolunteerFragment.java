package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.areResponse.Datum;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;
import com.votermanagement.superadmin.presenter.VolunteerPresenter;
import com.votermanagement.superadmin.presenter.VolunteerPresenterImpl;
import com.votermanagement.superadmin.view.adapter.CustomSpinnerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by insonix on 11/9/17.
 */

public class AddVolunteerFragment extends Fragment implements VolunteerView {


    @BindView(R.id.header)
    TextView header;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.cnfrm_pwd)
    EditText cnfrmPwd;
    @BindView(R.id.addVolunteer)
    Button addVolunteer;
    Unbinder unbinder;
    VolunteerPresenter mPresenter;
    @BindView(R.id.area_spinner)
    Spinner areaSpinner;
    private List<Datum> areaList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_add_volunteer, null);
        unbinder = ButterKnife.bind(this, view);
        mPresenter = new VolunteerPresenterImpl(getActivity(), this);
        mPresenter.getAreaList();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.addVolunteer)
    public void onViewClicked() {
        int areaInedx=areaSpinner.getSelectedItemPosition();
        if (validate())
            mPresenter.addVolunteer(name.getText().toString().trim(), userName.getText().toString().trim(), email.getText().toString().trim(),
                    password.getText().toString().trim(), phone.getText().toString(),areaList.get(areaInedx).toString());
    }

    private boolean validate() {

        if (userName.getText().toString().isEmpty()) {
            Snackbar.make(getView(), "Username can't be empty", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (name.getText().toString().isEmpty()) {
            Snackbar.make(getView(), "Name can't be empty", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (email.getText().toString().isEmpty()) {
            Snackbar.make(getView(), "Email can't be empty", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()) {
            Snackbar.make(getView(), "Invalid Email", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (phone.getText().toString().isEmpty()) {
            Snackbar.make(getView(), "Phone can't be empty", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (password.getText().toString().isEmpty() || cnfrmPwd.getText().toString().isEmpty()) {
            Snackbar.make(getView(), "Password can't be empty", Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!password.getText().toString().equalsIgnoreCase(cnfrmPwd.getText().toString())) {
            Snackbar.make(getView(), "Password mismatch", Snackbar.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    @Override
    public void onListLoaded(Response<VolunteerList> response) {
        //not needed
    }

    @Override
    public void onError(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showAreasList(Response<AreaList> response) {
        areaList=response.body().getData();
        areaSpinner.setAdapter(new CustomSpinnerAdapter(getActivity(),response.body().getData()));
    }
}
