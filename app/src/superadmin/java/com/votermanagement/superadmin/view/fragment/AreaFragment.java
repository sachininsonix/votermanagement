package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.DepttResponse.Datum;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.presenter.AddAreaDept;
import com.votermanagement.superadmin.presenter.AddAreaDeptImpl;
import com.votermanagement.superadmin.view.adapter.AreasAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class AreaFragment extends Fragment implements AddView {

    AddAreaDept mPresenter;
    @BindView(R.id.volunteerList)
    RecyclerView volunteerList;
    @BindView(R.id.floatBttn)
    FloatingActionButton floatBttn;
    Unbinder unbinder;
    private AreasAdapter mAdapter;


    public AreaFragment() {
        // Required empty public constructor
        mPresenter = new AddAreaDeptImpl(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_volunteer, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.getAreasList();
        return view;
    }

    @Override
    public void onSuccessAdd(String message) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getAreasList();
    }

    @Override
    public void onError(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onListLoaded(Response<AreaList> response) {
        mAdapter = new AreasAdapter(response.body().getData());
        volunteerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        volunteerList.setAdapter(mAdapter);
    }

    @Override
    public void onDepttListLoaded(List<Datum> data) {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.floatBttn)
    public void onViewClicked() {
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container,new AddAreaFragment()).addToBackStack("addArea").commit();
    }
}
