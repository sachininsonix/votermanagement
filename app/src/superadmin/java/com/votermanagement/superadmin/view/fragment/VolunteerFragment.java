package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;
import com.votermanagement.superadmin.presenter.VolunteerPresenter;
import com.votermanagement.superadmin.presenter.VolunteerPresenterImpl;
import com.votermanagement.superadmin.view.adapter.VolunteerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class VolunteerFragment extends Fragment implements VolunteerView {

    VolunteerPresenter mPresenter;
    @BindView(R.id.volunteerList)
    RecyclerView volunteerList;
    @BindView(R.id.floatBttn)
    FloatingActionButton floatBttn;
    Unbinder unbinder;
    private VolunteerAdapter mAdapter;


    public VolunteerFragment() {
        // Required empty public constructor
        mPresenter = new VolunteerPresenterImpl(getActivity(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_volunteer, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.getVolunteerList("3");
        return view;
    }

    @Override
    public void onListLoaded(Response<VolunteerList> response) {
        mAdapter = new VolunteerAdapter(response.body().getData());
        volunteerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        volunteerList.setAdapter(mAdapter);
    }

    @Override
    public void onError(String msg) {
        Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showAreasList(Response<AreaList> response) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.floatBttn)
    public void onViewClicked() {
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container,new AddVolunteerFragment()).addToBackStack("add").commit();
    }
}
