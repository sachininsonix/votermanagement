package com.votermanagement.superadmin.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.DepttResponse.Datum;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.presenter.AddAreaDept;
import com.votermanagement.superadmin.presenter.AddAreaDeptImpl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by insonix on 12/9/17.
 */

public class AddAreaFragment extends Fragment implements AddView{

    Unbinder unbinder;
    public static final String TAG = AddAreaFragment.class.getName();
    @BindView(R.id.addAreaBttn)
    Button addAreaBttn;
    @BindView(R.id.placeSelected)
    EditText placeSelected;
   PlaceAutocompleteFragment placeAutocompleteFragment;

    @Nullable
    private PlaceSelectionListener zzaRm;
    private AddAreaDept mPresenter;
    private String placeChosen,placeLatLng;
    private String completePlace;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_area, container, false);
        unbinder = ButterKnife.bind(this, view);
//        getLocation();
        mPresenter=new AddAreaDeptImpl(getActivity(),this);
        placeAutocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("In")
                .build();
        placeAutocompleteFragment.setFilter(typeFilter);
        placeAutocompleteFragment.setBoundsBias(new LatLngBounds(
                new LatLng(30.6700209, 76.690814), new LatLng(30.800142, 76.8362439)));
        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
//                placeSelected.setText(place.getName().toString());
                placeChosen=place.getName().toString();
                placeLatLng=place.getLatLng().toString();
                completePlace=place.getAddress().toString();
                Log.i(TAG, "onPlaceSelected: "+place.getAddress()+","+placeLatLng);
            }

            @Override
            public void onError(Status status) {
                Snackbar.make(getView(),"Some error occurred",Snackbar.LENGTH_LONG).show();
            }
        });

        addAreaBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: "+placeLatLng.substring(10,placeLatLng.length()-1));
                if(completePlace.contains("Chandigarh")){
                    mPresenter.addArea(completePlace,placeLatLng.substring(10,placeLatLng.length()-1));
                }else {
                    Snackbar.make(getView(),"Selected area out of region",Snackbar.LENGTH_LONG).show();
                }
            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSuccessAdd(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_LONG).show();
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container, new AreaFragment()).commit();
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onListLoaded(Response<AreaList> response) {

    }

    @Override
    public void onDepttListLoaded(List<Datum> data) {

    }
}
