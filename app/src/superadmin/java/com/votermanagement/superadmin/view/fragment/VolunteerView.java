package com.votermanagement.superadmin.view.fragment;

import com.votermanagement.main.activity.model.AreaResponse;
import com.votermanagement.superadmin.model.areResponse.AreaList;
import com.votermanagement.superadmin.model.volunteer.VolunteerList;

import retrofit2.Response;

/**
 * Created by insonix on 11/9/17.
 */

public interface VolunteerView {

    void onListLoaded(Response<VolunteerList> response);

    void onError(String msg);

    void showAreasList(Response<AreaList> response);
}
