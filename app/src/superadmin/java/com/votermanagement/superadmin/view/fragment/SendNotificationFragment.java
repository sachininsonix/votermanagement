package com.votermanagement.superadmin.view.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.presenter.SendNotificationPresenter;
import com.votermanagement.superadmin.presenter.SendNotificationPresenterImpl;
import com.votermanagement.superadmin.utils.CommonUtils;
import com.votermanagement.superadmin.view.activity.SuperHomeActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.votermanagement.main.activity.view.IdeaActivity.getPath;

/**
 * Created by insonix on 13/9/17.
 */

public class SendNotificationFragment extends Fragment implements SendView {

    private static final String TAG = SendNotificationFragment.class.getName();
    private static final int MY_PERMISSIONS_REQUEST = 121;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.userType)
    Spinner userType;
    @BindView(R.id.add_img)
    AppCompatCheckBox addImg;
    @BindView(R.id.message_img)
    ImageView messageImg;
    @BindView(R.id.send_noti_bttn)
    Button sendNotiBttn;
    Unbinder unbinder;
    @BindView(R.id.iamge_layout)
    LinearLayout iamgeLayout;
    private Bitmap imageBitmap;
    SendNotificationPresenter mPresenter;
    private SharedPreferences mSprefs;
    File file;
    String Notitype="text";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_notification, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter = new SendNotificationPresenterImpl(getActivity(), this);
        mSprefs = getActivity().getSharedPreferences("MyPrefs", MODE_PRIVATE);
        addImg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    iamgeLayout.setVisibility(View.VISIBLE);
                } else {
                    iamgeLayout.setVisibility(View.GONE);
                }
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getPickImageChooserIntent(final Activity ctx) {
        final String[] items = new String[]{"From Camera", "From SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 1);
                    }
                    dialog.cancel();
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                    dialog.cancel();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1 && data != null) {
                Uri selectedImageUri = data.getData( );
                String picturePath = getPath( getActivity( ).getApplicationContext( ), selectedImageUri );
                file = new File(picturePath);
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                imageBitmap = CommonUtils.getResizedBitmap(thumbnail, 200, 200);
                messageImg.setImageBitmap(imageBitmap);
                Notitype="image";
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Log.i(TAG, "onActivityResult: "+picturePath);
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                imageBitmap = CommonUtils.getResizedBitmap(thumbnail, 200, 200);
                messageImg.setImageBitmap(imageBitmap);
                file = new File(picturePath);
                Notitype="image";
            }
        }
    }

    @OnClick(R.id.iamge_layout)
    public void onMessageViewClicked() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            getPickImageChooserIntent(getActivity());
        } else{
            checkAndRequestPermissions();
        }
    }


    private boolean checkAndRequestPermissions() {
        int CameraPermission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);
        int ReadStoragePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (CameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST);
            return false;
        }
        return true;
    }



    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getPickImageChooserIntent(getActivity());
        } else {
            checkAndRequestPermissions();
        }
    }

    @OnClick(R.id.send_noti_bttn)
    public void onViewClicked() {
        if(CommonUtils.isNetworkAvailable(getActivity())){
            if(file==null){
                Notitype="text";
                imageBitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.dummy);
                try {
                    file = getFileFromDummy(imageBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mPresenter.sendNotification(mSprefs.getString("userId", ""), title.getText().toString().trim(), message.getText().toString(), String.valueOf(userType.getSelectedItemPosition()), file, file.getName(), Notitype);
            }else{
                mPresenter.sendNotification(mSprefs.getString("userId", ""), title.getText().toString().trim(), message.getText().toString(), String.valueOf(userType.getSelectedItemPosition()), file, file.getName(), Notitype);
            }
        }else{
            CommonUtils.networkErrorDialog(getActivity());
        }

    }

    private File getFileFromDummy(Bitmap imageBitmap) throws IOException {
        File f = new File(getActivity().getCacheDir(), "dummy.jpg");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

//Convert bitmap to byte array
        Bitmap bitmap = imageBitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }

    @Override
    public void SuccessFul(String s) {
        Snackbar.make(getView(),s,Snackbar.LENGTH_LONG).show();
        /*getActivity().startActivity(new Intent(getActivity(), SuperHomeActivity.class));
        getActivity().finish();*/

    }
}
