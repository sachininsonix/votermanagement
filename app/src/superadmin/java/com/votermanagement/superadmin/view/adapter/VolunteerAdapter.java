package com.votermanagement.superadmin.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.superadmin.model.volunteer.Datum;

import java.util.List;

/**
 * Created by insonix on 11/9/17.
 */

public class VolunteerAdapter extends RecyclerView.Adapter<VolunteerAdapter.VolunteerViewHolder> {

    List<Datum> mList;
    public VolunteerAdapter(List<Datum> data) {
        mList=data;
    }

    @Override
    public VolunteerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.volunteer_row_item, parent, false);

        return new VolunteerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VolunteerViewHolder holder, int position) {
        Datum movie = mList.get(position);
        holder.title.setText(movie.getUsername());
        holder.genre.setText(movie.getEmail());
        holder.year.setText(movie.getCreatedDate());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class VolunteerViewHolder extends ViewHolder {
        private final TextView title,genre,year;
        public VolunteerViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            genre = view.findViewById(R.id.genre);
            year = view.findViewById(R.id.year);
        }
    }
}
