package com.votermanagement.main.utils;

/**
 * Created by prashant on 15/11/16.
 */
public interface Constants {
    String regex =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    String myFormat = "dd-MM-yyyy"; //In which you need put here

    String USER_NAME = "name";
    String PASSWORD = "psw";
    String PHONE_NUMBER = "phone";
    String DEVICE_ID = "device_id";
    String DEVICE_TOKEN = "device_token";
    String DEVICE_TYPE = "device_type";
    String AREA = "area";
    String USER_ID = "userID";
    String ROLE = "role";
    String BASE_URL = "http://khersocialapp.insonix.com/index.php/webservice/";
    String INCOMPLETE_URL = BASE_URL + "assets/flat_images/";
    String FACEBOOK_FEED_BASE_URL="https://graph.facebook.com/";
    String FACEBOOK_NUMBER_OF_POST="100";
    String FACEBOOK_FIELDS="from,story,created_time,message,full_picture,likes.limit(1).summary(true),comments.limit(1).summary(true),shares";
    String FACEBOOK_TOKEN="1321707791291746|1e1c54421a69bf664ca6ca641bb081f1";

}
