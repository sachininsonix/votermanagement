package com.votermanagement.main.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by root on 8/6/17.
 */


    public class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private Bitmap bitmap ;
        private FileOutputStream fos;
        public GetImages(String requestUrl, String _imagename_) {
            this.requestUrl = requestUrl;
            this.imagename_ = _imagename_ ;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(!ImageStorage.checkifImageExists(imagename_))
            {
                ImageStorage.saveToSdCard(bitmap, imagename_);
            }
        }
    }
