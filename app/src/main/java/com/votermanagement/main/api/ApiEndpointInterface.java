package com.votermanagement.main.api;


import com.votermanagement.main.model.AreaListModel.AreaListResponse;
import com.votermanagement.main.model.commentlist.CommentListModel;
import com.votermanagement.main.model.commentsubmit.CommentSubmitModel;
import com.votermanagement.main.model.complaintlist.ComplaintListModel;
import com.votermanagement.main.model.deaprtment.DepartmentListResponse;
import com.votermanagement.main.model.facebook.FacebookFeedResponse;
import com.votermanagement.main.model.login.LoginResponse;
import com.votermanagement.main.model.notification.NotificationListModel;
import com.votermanagement.main.model.signup.SignupResponse;
import com.votermanagement.main.model.submitcomplaint.SubmitComplaintResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by android3 on 26/8/16.
 */
public interface ApiEndpointInterface {

    @GET("userlogin")
    Call<LoginResponse> login(@Query("username") String userName, @Query("password") String pwd, @Query("device_id") String device_id, @Query("device_token") String device_token, @Query("device_type") String device_type);


    @GET("signUp")
    Call<SignupResponse> signUp(@Query("name") String userName, @Query("psw") String psw, @Query("phone") String phone, @Query("device_id") String device_id, @Query("device_token") String device_token, @Query("device_type") String device_type, @Query("area") String area, @Query("lat") String lat, @Query("long") String lon);

    @GET("getAreas")
    Call<AreaListResponse> showAreaList();

    @GET("getUsersComplaint")
    Call<ComplaintListModel> showComplaintListAndStatus(@Query("user_id") String user_id);

    @GET("getAllDepartments")
    Call<DepartmentListResponse> showDepartmentList();

    @Multipart
    @POST("addComplaint")
    Call<SubmitComplaintResponse> submitComplaint(@Part("complaint_title") RequestBody complaintTitle, @Part("department_id") RequestBody departmentId, @Part MultipartBody.Part part, @Part("user_id") RequestBody userId, @Part("details") RequestBody details, @Part("filename") RequestBody filename, @Part("area_id") RequestBody area_id);

    @GET("getAllReply?")
    Call<CommentListModel> getAllComments(@Query("complaint_id") String complaint_id);

    @GET("addReplyToComplaint")
    Call<CommentSubmitModel> postComment(@Query("complaint_id") String complaint_id, @Query("message") String message, @Query("user_id") String user_id, @Query("status") String status);

    @GET("getComplaintsByStatus")
    Call<ComplaintListModel> getAreaComplaintList(@Query("area_id") String area_id);

    @GET("getUserNotifications")
    Call<NotificationListModel> getNotificationList(@Query("user_id") String user_id);

    @GET("kirronkher14/feed")
    Call<FacebookFeedResponse> getAllFacebookFeed(@Query("limit") String limit, @Query("fields") String field, @Query("access_token") String access_token);
}
