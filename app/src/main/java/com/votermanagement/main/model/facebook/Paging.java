
package com.votermanagement.main.model.facebook;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Paging {

    @SerializedName("cursors")
    private Cursors mCursors;
    @SerializedName("next")
    private String mNext;

    public Cursors getCursors() {
        return mCursors;
    }

    public void setCursors(Cursors cursors) {
        mCursors = cursors;
    }

    public String getNext() {
        return mNext;
    }

    public void setNext(String next) {
        mNext = next;
    }

}
