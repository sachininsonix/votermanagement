package com.votermanagement.main.model.AreaListModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("area_name")
	private String areaName;

	@SerializedName("lng")
	private String lng;

	@SerializedName("assigned_pradhan_id")
	private String assignedPradhanId;

	@SerializedName("id")
	private String id;

	@SerializedName("lat")
	private String lat;

	public void setAreaName(String areaName){
		this.areaName = areaName;
	}

	public String getAreaName(){
		return areaName;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setAssignedPradhanId(String assignedPradhanId){
		this.assignedPradhanId = assignedPradhanId;
	}

	public String getAssignedPradhanId(){
		return assignedPradhanId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	@Override
 	public String toString(){
		return 
			"ComplaintListData{" +
			"area_name = '" + areaName + '\'' + 
			",lng = '" + lng + '\'' + 
			",assigned_pradhan_id = '" + assignedPradhanId + '\'' + 
			",id = '" + id + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}