package com.votermanagement.main.model.signup;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("area")
	private String area;

	@SerializedName("current_lat")
	private String currentLat;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("phone")
	private String phone;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("current_long")
	private String currentLong;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("userId")
	private int userId;

	@SerializedName("username")
	private String username;

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setCurrentLat(String currentLat){
		this.currentLat = currentLat;
	}

	public String getCurrentLat(){
		return currentLat;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setCurrentLong(String currentLong){
		this.currentLong = currentLong;
	}

	public String getCurrentLong(){
		return currentLong;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"area = '" + area + '\'' + 
			",current_lat = '" + currentLat + '\'' + 
			",device_id = '" + deviceId + '\'' + 
			",phone = '" + phone + '\'' + 
			",device_token = '" + deviceToken + '\'' + 
			",current_long = '" + currentLong + '\'' + 
			",device_type = '" + deviceType + '\'' + 
			",userId = '" + userId + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}