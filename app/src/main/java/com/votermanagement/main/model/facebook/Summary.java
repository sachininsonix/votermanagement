
package com.votermanagement.main.model.facebook;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Summary {

    @SerializedName("can_comment")
    private Boolean mCanComment;
    @SerializedName("can_like")
    private Boolean mCanLike;
    @SerializedName("has_liked")
    private Boolean mHasLiked;
    @SerializedName("order")
    private String mOrder;
    @SerializedName("total_count")
    private Long mTotalCount;

    public Boolean getCanComment() {
        return mCanComment;
    }

    public void setCanComment(Boolean canComment) {
        mCanComment = canComment;
    }

    public Boolean getCanLike() {
        return mCanLike;
    }

    public void setCanLike(Boolean canLike) {
        mCanLike = canLike;
    }

    public Boolean getHasLiked() {
        return mHasLiked;
    }

    public void setHasLiked(Boolean hasLiked) {
        mHasLiked = hasLiked;
    }

    public String getOrder() {
        return mOrder;
    }

    public void setOrder(String order) {
        mOrder = order;
    }

    public Long getTotalCount() {
        return mTotalCount;
    }

    public void setTotalCount(Long totalCount) {
        mTotalCount = totalCount;
    }

}
