
package com.votermanagement.main.model.facebook;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Comments {

    @SerializedName("data")
    private List<DataItem> mData;
    @SerializedName("paging")
    private Paging mPaging;
    @SerializedName("summary")
    private Summary mSummary;

    public List<DataItem> getData() {
        return mData;
    }

    public void setData(List<DataItem> data) {
        mData = data;
    }

    public Paging getPaging() {
        return mPaging;
    }

    public void setPaging(Paging paging) {
        mPaging = paging;
    }

    public Summary getSummary() {
        return mSummary;
    }

    public void setSummary(Summary summary) {
        mSummary = summary;
    }

}
