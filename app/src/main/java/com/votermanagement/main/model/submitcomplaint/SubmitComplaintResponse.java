package com.votermanagement.main.model.submitcomplaint;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SubmitComplaintResponse {

	@SerializedName("complaint_id")
	private int complaintId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setComplaintId(int complaintId){
		this.complaintId = complaintId;
	}

	public int getComplaintId(){
		return complaintId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AddComplaintResponse{" + 
			"complaint_id = '" + complaintId + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}