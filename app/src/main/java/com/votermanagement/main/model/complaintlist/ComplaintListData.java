package com.votermanagement.main.model.complaintlist;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ComplaintListData implements Serializable{

	@SerializedName("area_name")
	private Object areaName;

	@SerializedName("complaint_id")
	private String complaintId;

	@SerializedName("complaint_status")
	private String complaintStatus;

	@SerializedName("fillename")
	private String fillename;

	@SerializedName("attachment")
	private String attachment;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("complaint_date")
	private String complaintDate;

	@SerializedName("department_name")
	private String departmentName;

	@SerializedName("complaint_title")
	private String complaintTitle;

	@SerializedName("details")
	private String details;

	@SerializedName("reply")
	private List<Object> reply;

	@SerializedName("username")
	private String username;

	public void setAreaName(Object areaName){
		this.areaName = areaName;
	}

	public Object getAreaName(){
		return areaName;
	}

	public void setComplaintId(String complaintId){
		this.complaintId = complaintId;
	}

	public String getComplaintId(){
		return complaintId;
	}

	public void setComplaintStatus(String complaintStatus){
		this.complaintStatus = complaintStatus;
	}

	public String getComplaintStatus(){
		return complaintStatus;
	}

	public void setFillename(String fillename){
		this.fillename = fillename;
	}

	public String getFillename(){
		return fillename;
	}

	public void setAttachment(String attachment){
		this.attachment = attachment;
	}

	public String getAttachment(){
		return attachment;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setComplaintDate(String complaintDate){
		this.complaintDate = complaintDate;
	}

	public String getComplaintDate(){
		return complaintDate;
	}

	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}

	public String getDepartmentName(){
		return departmentName;
	}

	public void setComplaintTitle(String complaintTitle){
		this.complaintTitle = complaintTitle;
	}

	public String getComplaintTitle(){
		return complaintTitle;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public String getDetails(){
		return details;
	}

	public void setReply(List<Object> reply){
		this.reply = reply;
	}

	public List<Object> getReply(){
		return reply;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"ComplaintListData{" +
			"area_name = '" + areaName + '\'' + 
			",complaint_id = '" + complaintId + '\'' + 
			",complaint_status = '" + complaintStatus + '\'' + 
			",fillename = '" + fillename + '\'' + 
			",attachment = '" + attachment + '\'' + 
			",user_id = '" + userId + '\'' + 
			",complaint_date = '" + complaintDate + '\'' + 
			",department_name = '" + departmentName + '\'' + 
			",complaint_title = '" + complaintTitle + '\'' + 
			",details = '" + details + '\'' + 
			",reply = '" + reply + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}