package com.votermanagement.main.model.facebook;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SummaryLikes{

	@SerializedName("has_liked")
	private boolean hasLiked;

	@SerializedName("can_like")
	private boolean canLike;

	@SerializedName("total_count")
	private int totalCount;

	public void setHasLiked(boolean hasLiked){
		this.hasLiked = hasLiked;
	}

	public boolean isHasLiked(){
		return hasLiked;
	}

	public void setCanLike(boolean canLike){
		this.canLike = canLike;
	}

	public boolean isCanLike(){
		return canLike;
	}

	public void setTotalCount(int totalCount){
		this.totalCount = totalCount;
	}

	public int getTotalCount(){
		return totalCount;
	}

	@Override
 	public String toString(){
		return 
			"SummaryLikes{" + 
			"has_liked = '" + hasLiked + '\'' + 
			",can_like = '" + canLike + '\'' + 
			",total_count = '" + totalCount + '\'' + 
			"}";
		}
}