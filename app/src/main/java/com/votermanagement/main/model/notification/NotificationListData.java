package com.votermanagement.main.model.notification;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class NotificationListData implements Serializable{

	@SerializedName("image")
	private String image;

	@SerializedName("datetime")
	private String datetime;

	@SerializedName("from_id")
	private String fromId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("id")
	private String id;

	@SerializedName("title")
	private String title;

	@SerializedName("message")
	private String message;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setDatetime(String datetime){
		this.datetime = datetime;
	}

	public String getDatetime(){
		return datetime;
	}

	public void setFromId(String fromId){
		this.fromId = fromId;
	}

	public String getFromId(){
		return fromId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"NotificationListData{" +
			"image = '" + image + '\'' + 
			",datetime = '" + datetime + '\'' + 
			",from_id = '" + fromId + '\'' + 
			",user_id = '" + userId + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}