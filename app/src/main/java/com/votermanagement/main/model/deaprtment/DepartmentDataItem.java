package com.votermanagement.main.model.deaprtment;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class DepartmentDataItem {

	@SerializedName("department_name")
	private String departmentName;

	@SerializedName("id")
	private String id;

	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}

	public String getDepartmentName(){
		return departmentName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DepartmentDataItem{" +
			"department_name = '" + departmentName + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}