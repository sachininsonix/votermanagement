package com.votermanagement.main.model.commentsubmit;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CommentSubmitModel{

	@SerializedName("message_id")
	private int messageId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setMessageId(int messageId){
		this.messageId = messageId;
	}

	public int getMessageId(){
		return messageId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CommentSubmitModel{" + 
			"message_id = '" + messageId + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}