package com.votermanagement.main.model.commentlist;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CommentListData {

	@SerializedName("complaint_id")
	private String complaintId;

	@SerializedName("complaint_status")
	private String complaintStatus;

	@SerializedName("reply_id")
	private String replyId;

	@SerializedName("reply_status")
	private String replyStatus;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("message")
	private String message;

	@SerializedName("reply_date")
	private String replyDate;

	@SerializedName("username")
	private String username;

	public void setComplaintId(String complaintId){
		this.complaintId = complaintId;
	}

	public String getComplaintId(){
		return complaintId;
	}

	public void setComplaintStatus(String complaintStatus){
		this.complaintStatus = complaintStatus;
	}

	public String getComplaintStatus(){
		return complaintStatus;
	}

	public void setReplyId(String replyId){
		this.replyId = replyId;
	}

	public String getReplyId(){
		return replyId;
	}

	public void setReplyStatus(String replyStatus){
		this.replyStatus = replyStatus;
	}

	public String getReplyStatus(){
		return replyStatus;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setReplyDate(String replyDate){
		this.replyDate = replyDate;
	}

	public String getReplyDate(){
		return replyDate;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"CommentListData{" +
			"complaint_id = '" + complaintId + '\'' + 
			",complaint_status = '" + complaintStatus + '\'' + 
			",reply_id = '" + replyId + '\'' + 
			",reply_status = '" + replyStatus + '\'' + 
			",user_id = '" + userId + '\'' + 
			",message = '" + message + '\'' + 
			",reply_date = '" + replyDate + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}