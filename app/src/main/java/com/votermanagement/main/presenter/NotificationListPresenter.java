package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.notification.NotificationListData;
import com.votermanagement.main.model.notification.NotificationListModel;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 19/9/17.
 */

public class NotificationListPresenter implements Constants {

    private final Context context;
    private final NotificationListPresenterListener mListener;
    private final ApiClient apiClient;
    private final ProgressBar loader;
    //private final ArrayList<NotificationListData> NotificationData = new ArrayList<>();

    public interface NotificationListPresenterListener {
//        void setNotificationList(ArrayList<NotificationListData> data);
        void setNotificationList(List<NotificationListData> data);

        void showEmptyNotificationList(String message);
    }

    public NotificationListPresenter(Context context, NotificationListPresenterListener mListener, ProgressBar loader) {
        this.context = context;
        this.mListener = mListener;
        this.loader = loader;
        apiClient = new ApiClient();
    }

    public void getAllNotification() {
        loader.setVisibility(View.VISIBLE);
        apiClient.createService(BASE_URL)
                .getNotificationList(Singleton.getInstance().getValue(context, USER_ID))
                .enqueue(new Callback<NotificationListModel>() {
                    @Override
                    public void onResponse(Call<NotificationListModel> call, Response<NotificationListModel> response) {
                        Log.i(TAG, "NotificationResponse: "+response.body().getData());
                        if (response.body().getStatus().equals("true")) {
                            //setNotificationData(response.body().getData());
                            mListener.setNotificationList(response.body().getData());

                        } else {
                            mListener.showEmptyNotificationList(response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationListModel> call, Throwable t) {
                        loader.setVisibility(View.GONE);
                        Log.i(TAG, "onFailure: " + t.getMessage());
                    }
                });
    }

  /*  private void setNotificationData(List<NotificationListData> data) {
        NotificationListData notificationData = new NotificationListData();
        for (int i = 0; i < data.size(); i++) {
            notificationData.setDatetime(data.get(i).getDatetime());
            notificationData.setFromId(data.get(i).getFromId());
            notificationData.setId(data.get(i).getId());
            notificationData.setImage(data.get(i).getImage());
            notificationData.setMessage(data.get(i).getMessage());
            notificationData.setTitle(data.get(i).getTitle());
            notificationData.setUserId(data.get(i).getUserId());
            NotificationData.add(notificationData);
            mListener.setNotificationList(NotificationData);
        }

    }*/
}
