package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.facebook.DataItem;
import com.votermanagement.main.model.facebook.FacebookFeedResponse;
import com.votermanagement.main.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.twitter.sdk.android.core.TwitterCore.TAG;

/**
 * Created by insonix on 20/9/17.
 */

public class FacebookFeedPresenter implements Constants {

    private final Context context;
    private final FacebookFeedPresenterListener mListener;
    private final ApiClient apiClient;
    private final ProgressBar loader;
//    ArrayList<DataItem> facebookDataArray = new ArrayList<>();

    public interface FacebookFeedPresenterListener {

        void setFacebookFeed(List<DataItem> facebookDataArray);
    }

    public FacebookFeedPresenter(Context context, FacebookFeedPresenterListener mListener, ProgressBar loader) {
        this.context = context;
        this.mListener = mListener;
        this.loader = loader;
        apiClient = new ApiClient();

    }

    public void getAllFeed() {
        loader.setVisibility(View.VISIBLE);
        apiClient.createService(FACEBOOK_FEED_BASE_URL)
                .getAllFacebookFeed(FACEBOOK_NUMBER_OF_POST,FACEBOOK_FIELDS,FACEBOOK_TOKEN )
                .enqueue(new Callback<FacebookFeedResponse>() {
                    @Override
                    public void onResponse(Call<FacebookFeedResponse> call, Response<FacebookFeedResponse> response) {
                        Log.i(TAG, "FacebookFeedResponse: " + response.body().getData());
                        loader.setVisibility(View.GONE);

                            mListener.setFacebookFeed(response.body().getData());
                            //setData(response.body().getData());


                    }

                    @Override
                    public void onFailure(Call<FacebookFeedResponse> call, Throwable t) {
                        Log.i(TAG, "FacebookFeedFailure: " + t.getMessage());
                        loader.setVisibility(View.GONE);
                    }
                });
    }

   /* private void setData(List<DataItem> data) {

        DataItem facebookData = new DataItem();
        facebookDataArray.clear();
        Log.i(TAG, "setData: " + data.size());
        for (int i = 0; i < data.size(); i++) {

            facebookData.setComments(data.get(i).getComments());
           facebookData.setCreatedTime(data.get(i).getCreatedTime());
            facebookData.setFrom(data.get(i).getFrom());
            facebookData.setId(data.get(i).getId());
            facebookData.setLikes(data.get(i).getLikes());
            facebookData.setMessage(data.get(i).getMessage());
            facebookData.setPicture(data.get(i).getPicture());
            facebookData.setName(data.get(i).getName());
            facebookData.setStory(data.get(i).getStory());
            facebookDataArray.add(facebookData);
           if(TextUtils.isEmpty(data.get(i).getMessage())){
               facebookData.setMessage(null);
           }else {
               facebookData.setMessage(data.get(i).getMessage());
           }
            facebookData.setMessage(data.get(i).getMessage());
            Log.i(TAG, "Data: " + data.get(i).getMessage());
            facebookDataArray.add(facebookData);
        }

    *//*    for (int i = 0; i < facebookDataArray.size(); i++) {
            Log.i(TAG, "check: " + facebookDataArray.get(i).getMessage());
        }*//*
        Log.i(TAG, "FacebookArraySize: " + facebookDataArray.size());
        mListener.setFacebookFeed(facebookDataArray);

    }*/
}
