package com.votermanagement.main.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.login.LoginResponse;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.MySharedPreferences;
import com.votermanagement.main.utils.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 20/12/16.
 */

public class UserLoginPresenterImpl implements Constants {
    private final Context context;
    private final UserLoginPresenterListener mListener;
    private final ApiClient apiClient;
    MySharedPreferences mySharedPreferences;

    public interface UserLoginPresenterListener {
        void onLoginSuccess();

        void showUserNameError();

        void showPasswordError();
    }

    public UserLoginPresenterImpl(UserLoginPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.apiClient = new ApiClient();
    }

    public void login(String username, String password, String device_id, String device_token, String device_type) {


        if (onValidate(username, password)) {
            // Singleton.getInstance().showProgressDialog(context);
            Singleton.getInstance().showProgressDialog(context);
            apiClient
                    .createService(BASE_URL)
                    .login(username, password, device_id, device_token, device_type)
                    .enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            Singleton.getInstance().dismissDialog();
                            //Log.i("TAG", "onResponse: " + response.body());
                            if (response.body().getStatus().equals("true")) {

                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                if (response.body().getStatus().equals("true")) {

                                    Singleton.getInstance().saveValue(context, USER_ID, response.body().getUserId());
                                    Singleton.getInstance().saveValue(context, USER_NAME, response.body().getUsername());
                                    Singleton.getInstance().saveValue(context, AREA, response.body().getAreaId());
                                    Singleton.getInstance().saveValue(context, ROLE, response.body().getRole());

                                }
                                mListener.onLoginSuccess();
                            } else {
                                Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                            }
                            // Singleton.getInstance().dismissDialog();
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Singleton.getInstance().dismissDialog();
                            Log.e("response", "failure" + t);
                            // Singleton.getInstance().dismissDialog();
                            try {
                                throw new InterruptedException("Erro na comunicação com o servidor!");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }

        /*if (onValidate(username, password)) {
            // Singleton.getInstance().showProgressDialog(context);
            apiClient
                    .createService(BASE_URL.trim())
                    .login(username, password, android_id, token, DEVICE_TYPE)
                    .enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            Log.e("response", "success");
                            Log.e("response", ":: " + response.raw());
                            if (response.body().getStatus().equals("true")) {
                                //   if (response.body().getUserdata()!=null){
                                for (int i = 0; i < response.body().getData().size(); i++) {







                                mListener.onLoginSuccess();
                            } else {
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            // Singleton.getInstance().dismissDialog();
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Log.e("response", "failure" + t);
                            //Singleton.getInstance().dismissDialog();
                            try {
                                throw new InterruptedException("Erro na comunicação com o servidor!");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }*/
    }

    private boolean onValidate(String username, String password) {

        if (username.isEmpty()) {
            mListener.showUserNameError();
            return false;
        } else if (password.isEmpty()) {
            mListener.showPasswordError();
            return false;
        }
        return true;
    }

    private static boolean isValidEmail(String phone) {
        return !TextUtils.isEmpty(phone) && Patterns.PHONE.matcher(phone).matches();
    }


}
