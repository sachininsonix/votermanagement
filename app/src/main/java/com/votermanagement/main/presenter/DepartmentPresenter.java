package com.votermanagement.main.presenter;

/**
 * Created by Sandeep on 9/17/2017.
 */

public interface DepartmentPresenter {
    void getDepartmentList();
}
