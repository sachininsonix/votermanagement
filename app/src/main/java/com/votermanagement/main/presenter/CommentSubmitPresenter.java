package com.votermanagement.main.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.commentsubmit.CommentSubmitModel;
import com.votermanagement.main.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 18/9/17.
 */

public class CommentSubmitPresenter implements Constants {
    private final ApiClient apiClient;
    private final Context context;
    private final CommentSubmitPresenterListener mListener;


    public interface CommentSubmitPresenterListener {
        void onSuccessfulSubmit(String complaint_id);

        void showEmptyMessageError();
    }

    public CommentSubmitPresenter(Context context, CommentSubmitPresenterListener mListener) {
        this.context = context;
        this.mListener = mListener;
        apiClient = new ApiClient();
    }

    public void submitComment(final String complaint_id, String message, String user_id, final String complaint_status) {
        if (onValidate(message)) {
            apiClient.createService(BASE_URL)
                    .postComment(complaint_id, message, user_id, complaint_status)
                    .enqueue(new Callback<CommentSubmitModel>() {
                        @Override
                        public void onResponse(Call<CommentSubmitModel> call, Response<CommentSubmitModel> response) {
                            if (response.body().getStatus().equals("true")) {
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                mListener.onSuccessfulSubmit(complaint_id);
                            }
                        }

                        @Override
                        public void onFailure(Call<CommentSubmitModel> call, Throwable t) {
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });

        }
    }

    public boolean onValidate(String message) {
        if (TextUtils.isEmpty(message)) {
            mListener.showEmptyMessageError();
            return false;
        } else {
            return true;
        }
    }
}
