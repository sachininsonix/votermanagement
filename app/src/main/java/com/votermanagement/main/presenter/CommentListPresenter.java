package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.commentlist.CommentListData;
import com.votermanagement.main.model.commentlist.CommentListModel;
import com.votermanagement.main.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by insonix on 18/9/17.
 */

public class CommentListPresenter implements Constants {

    private static final String TAG = "CommentListPresenter";
    private final Context context;
    private final ReplyPresenterListener mListener;
    private final ApiClient apiClient;
    private final ProgressBar loader;

    private ArrayList<CommentListData> ReplyDataArray = new ArrayList<>();

    public interface ReplyPresenterListener {
        void setCommentList(ArrayList<CommentListData> replyDataArray);

        void onEmptyCommentList(String message);

    }

    public CommentListPresenter(Context context, ReplyPresenterListener mListener,ProgressBar loader) {
        this.context = context;
        this.mListener = mListener;
        apiClient = new ApiClient();
        this.loader=loader;
        ;
    }

    public void getAllComment(String complaint_id) {
        loader.setVisibility(View.VISIBLE);
        apiClient.createService(BASE_URL)
                .getAllComments(complaint_id)
                .enqueue(new Callback<CommentListModel>() {
                    @Override
                    public void onResponse(Call<CommentListModel> call, Response<CommentListModel> response) {
                       loader.setVisibility(View.GONE);
                        if (response.body().getStatus().equals("true")) {

                            setCommentData(response.body().getData());

                        } else {
                            mListener.onEmptyCommentList(response.body().getMessage());
                        }

                    }

                    @Override
                    public void onFailure(Call<CommentListModel> call, Throwable t) {
                        loader.setVisibility(View.GONE);
                    }
                });
    }

    private void setCommentData(List<CommentListData> data) {
        ReplyDataArray.clear();

        for (int i = 0; i < data.size(); i++) {
            CommentListData replyData = new CommentListData();
            replyData.setComplaintId(data.get(i).getComplaintId());
            replyData.setMessage(data.get(i).getMessage());
            replyData.setComplaintStatus(data.get(i).getComplaintStatus());
            replyData.setReplyDate(data.get(i).getReplyDate());
            replyData.setReplyId(data.get(i).getReplyId());
            replyData.setReplyStatus(data.get(i).getReplyStatus());
            replyData.setUserId(data.get(i).getUserId());
            replyData.setUsername(data.get(i).getUsername());
            ReplyDataArray.add(replyData);
        }
        Log.i(TAG, "ReplyDataArraySize: " + ReplyDataArray.size());
        mListener.setCommentList(ReplyDataArray);
    }
}
