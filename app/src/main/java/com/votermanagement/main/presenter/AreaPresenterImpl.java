package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.AreaListModel.AreaListResponse;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;
import com.votermanagement.main.view.AreaResponseViews;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by insonix on 29/8/17.
 */

public class AreaPresenterImpl implements AreaPresenter,Constants {
    private static final String TAG ="AreaPresenterImpl" ;
    private final ApiClient apiClient;
    private final Context context;
    private final AreaResponseViews areaResponseViews;




    public AreaPresenterImpl(AreaResponseViews areaResponseViews, Context context) {
        this.areaResponseViews = areaResponseViews;
        this.context = context;
        this.apiClient=new ApiClient();
    }

    public void getAreaList(){
        Singleton.getInstance().showProgressDialog(context);
        apiClient.createService(BASE_URL)
                .showAreaList()
                .enqueue(new Callback<AreaListResponse>() {
                    @Override
                    public void onResponse(Call<AreaListResponse> call, Response<AreaListResponse> response) {

                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            Log.i(TAG, "onResponse: "+response.body().getData());
                            areaResponseViews.getAreaViews(response.body().getData());
                            Singleton.getInstance().dismissDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<AreaListResponse> call, Throwable t) {
                        Log.i(TAG, "onFailure: "+t.getMessage());
                        Singleton.getInstance().dismissDialog();
                    }
                });


    }






}
