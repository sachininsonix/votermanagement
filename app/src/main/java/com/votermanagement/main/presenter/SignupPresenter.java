package com.votermanagement.main.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.signup.SignupResponse;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by root on 3/2/17.
 */

public class SignupPresenter implements Constants {

    private final Context context;
    private final SignupPresenterListener mListener;
    private final ApiClient apiClient;



    public interface SignupPresenterListener {

        void onSuccessfulRegistration();

        void showAreaError();

        void showPasswordError();

        void showUserNameError();



        void showPhoneNumberError();

        void showPhoneNumberLengthError();



        void showPasswordLengthError();



    }


    public SignupPresenter(SignupPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.apiClient = new ApiClient();
    }


    public void signUp(String username, String password, String phone, String device_id, String device_token, String device_type, String area_id, String latitude, String longitude) {
        Log.i("TAG", "signUp: "+username);
        if (onValidate(username, password, phone,area_id )) {
           // Singleton.getInstance().showProgressDialog(context);
            Singleton.getInstance().showProgressDialog(context);
            apiClient
                    .createService(BASE_URL)
                    .signUp(username, password, phone, device_id, device_token, device_type, area_id, latitude, longitude)
                    .enqueue(new Callback<SignupResponse>() {

                        @Override
                        public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {

                            Log.e("response", "success");
                            Log.e("response", ":: " + response.raw().request().url());

                            if (response.body().getStatus().equals("true")) {
                                Log.i("TAG", "onResponse: " + response.body().getMessage());
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                mListener.onSuccessfulRegistration();
                            } else {
                                Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                            }
                            Singleton.getInstance().dismissDialog();
                        }

                        @Override
                        public void onFailure(Call<SignupResponse> call, Throwable t) {

                            Log.e("response", "failure");
                            Singleton.getInstance().dismissDialog();
                        }
                    });
        }

    }


    private boolean onValidate(String username, String password, String phone, String area_id) {
        if (username.isEmpty()) {
            mListener.showUserNameError();
            return false;
        }
        else if (password.isEmpty()) {
            mListener.showPasswordError();
            return false;
        } else if (password.length() < 6) {
            mListener.showPasswordLengthError();
            return false;
        }
         else if (phone.isEmpty()) {
            mListener.showPhoneNumberError();
            return false;
        } else if (phone.length() < 10) {
            Log.i("TAG", "onValidate: "+phone);
            mListener.showPhoneNumberLengthError();
            return false;
        }else if(area_id.isEmpty()){
            mListener.showAreaError();
            return false;
        }

        return true;
    }

    private static boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && Patterns.PHONE.matcher(phone).matches();
    }
}