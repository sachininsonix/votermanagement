package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.complaintlist.ComplaintListData;
import com.votermanagement.main.model.complaintlist.ComplaintListModel;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 15/9/17.
 */

public class ComplaintListPresenterImpl implements Constants {
    private final ApiClient apiClient;
    private final Context context;
    private final ProgressBar loader;
    private final ComplaintListPresenterListener mListener;
    ArrayList<ComplaintListData> ComplaintList=new ArrayList<>();


    public interface ComplaintListPresenterListener {
//        void setComplaintList(ArrayList<ComplaintListData> complaintList);
        void setComplaintList(List<ComplaintListData> complaintList);
        void emptyList(String message);
    }

    public ComplaintListPresenterImpl(Context context, ComplaintListPresenterListener complaintListPresenterListener,ProgressBar loader) {
        this.context = context;
        this.mListener = complaintListPresenterListener;
        this.loader=loader;
        apiClient = new ApiClient();

    }
    public void getcomplaintList(){
        loader.setVisibility(View.VISIBLE);
        // role=0 user,role=1=super_admin,role=2=volunteer
        Log.i(TAG, "Role: "+ Singleton.getInstance().getValue(context,ROLE));
        if(Singleton.getInstance().getValue(context,ROLE).equals("0")){
            apiClient.createService(BASE_URL)
                    .showComplaintListAndStatus(Singleton.getInstance().getValue(context,USER_ID))
                    .enqueue(new Callback<ComplaintListModel>() {
                        @Override
                        public void onResponse(Call<ComplaintListModel> call, Response<ComplaintListModel> response) {
                            if(response.body().getStatus().equals("true")){
                                 /*Gson gson=new Gson();
                            String data=gson.toJson(response.body());
                            Log.i(TAG, "onResponse: "+data);*/
                               // setData(response.body());
                                mListener.setComplaintList(response.body().getData());
                            }else {
                                mListener.emptyList(response.body().getMessage());
                            }

                        }

                        @Override
                        public void onFailure(Call<ComplaintListModel> call, Throwable t) {
                            loader.setVisibility(View.GONE);
                            try {
                                throw new InterruptedException(t.getMessage());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }else if(Singleton.getInstance().getValue(context,ROLE).equals("2")){
                apiClient.createService(BASE_URL)
                        .getAreaComplaintList(Singleton.getInstance().getValue(context,AREA))
                        .enqueue(new Callback<ComplaintListModel>() {
                            @Override
                            public void onResponse(Call<ComplaintListModel> call, Response<ComplaintListModel> response) {
                                if(response.body().getStatus().equals("true")){
                                    mListener.setComplaintList(response.body().getData());
                                }else {
                                    mListener.emptyList(response.body().getMessage());
                                }
                                //setData(response.body());
                            }

                            @Override
                            public void onFailure(Call<ComplaintListModel> call, Throwable t) {
                                loader.setVisibility(View.GONE);
                                try {
                                    throw new InterruptedException(t.getMessage());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        });
        }else {

            mListener.emptyList("This is super admin");
        }

    }

   /* private void setData(ComplaintListModel data) {
        if(data.getStatus().equals("true")){
            ComplaintList.clear();
            for(int i=0;i<data.getData().size();i++){
                ComplaintListData complaintList=new ComplaintListData();
                complaintList.setAreaName(data.getData().get(i).getAreaName());
                complaintList.setAttachment(data.getData().get(i).getAttachment());
                complaintList.setComplaintDate(data.getData().get(i).getComplaintDate());
                complaintList.setComplaintId(data.getData().get(i).getComplaintId());
                complaintList.setComplaintStatus(data.getData().get(i).getComplaintStatus());
                complaintList.setComplaintTitle(data.getData().get(i).getComplaintTitle());
                complaintList.setDepartmentName(data.getData().get(i).getDepartmentName());
                complaintList.setDetails(data.getData().get(i).getDetails());
                complaintList.setFillename(data.getData().get(i).getFillename());
                complaintList.setUserId(data.getData().get(i).getUserId());
                complaintList.setUsername(data.getData().get(i).getUsername());
                complaintList.setReply(data.getData().get(i).getReply());
                ComplaintList.add(complaintList);

            }
            mListener.setComplaintList(ComplaintList);

        }else {
            mListener.emptyList(data.getMessage());
        }
    }*/

}