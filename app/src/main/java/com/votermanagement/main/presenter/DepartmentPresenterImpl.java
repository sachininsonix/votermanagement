package com.votermanagement.main.presenter;

import android.content.Context;
import android.util.Log;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.deaprtment.DepartmentListResponse;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.view.DepartmentResponseView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Sandeep on 9/17/2017.
 */

public class DepartmentPresenterImpl implements DepartmentPresenter,Constants {
    private final Context context;
    private final ApiClient apiClient;
    private final DepartmentResponseView departmentResponseView;

    public DepartmentPresenterImpl(Context context,DepartmentResponseView departmentResponseView){
        this.context=context;
        this.departmentResponseView=departmentResponseView;
        apiClient=new ApiClient();
    }
    @Override
    public void getDepartmentList() {
            apiClient.createService(BASE_URL)
                    .showDepartmentList()
                    .enqueue(new Callback<DepartmentListResponse>() {
                        @Override
                        public void onResponse(Call<DepartmentListResponse> call, Response<DepartmentListResponse> response) {
                            Log.i(TAG, "onResponse: "+response.body().getData());
                            if(response.body().getStatus().equals("true")){

                                departmentResponseView.getDepartmentViews(response.body().getData());
                            }

                        }

                        @Override
                        public void onFailure(Call<DepartmentListResponse> call, Throwable t) {
                            Log.i(TAG, "onFailure: "+t.getMessage());
                        }
                    });
    }
}
