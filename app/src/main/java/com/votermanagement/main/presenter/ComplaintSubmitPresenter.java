package com.votermanagement.main.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.votermanagement.main.api.ApiClient;
import com.votermanagement.main.model.submitcomplaint.SubmitComplaintResponse;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.GetImages;
import com.votermanagement.main.utils.ImageStorage;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by insonix on 18/9/17.
 */

public class ComplaintSubmitPresenter implements Constants {
    private static final String TAG="ComplaintSubmitPresenter";

    private final Context context;
    private final ApiClient apiClient;
    private final ComplaintSubmitPresenterListener mListener;
    private RequestBody complaint_title, complaint_department_id, complaint_user_id, complaint_details, complaint_file_name, complaint_area_id;
    private MultipartBody.Part body;
    GetImages getImages;
    File file1;
    public interface ComplaintSubmitPresenterListener{
        void onSuccessfulSubmit();

        void showTitleBlankError();

        void showDetailBlankError();


    }
    public ComplaintSubmitPresenter(Context context,ComplaintSubmitPresenterListener mListener){
        this.context=context;
        this.mListener=mListener;
        apiClient=new ApiClient();
    }
    public void submitComplaint(String title1, String departmentId1, String mediaPath, String userId1,String details1, String area_id){
        if (!TextUtils.isEmpty(mediaPath)) {
            file1 = new File(mediaPath);

        } else {
            getImages = new GetImages(INCOMPLETE_URL + "complaint.png", "complaint");
            getImages.execute();
            file1 = ImageStorage.getImage("complaint.png");

        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
        body = MultipartBody.Part.createFormData("attachment", file1.getName(), requestFile);
        complaint_file_name = RequestBody.create(MediaType.parse("text/plain"), file1.getName());
        complaint_title = RequestBody.create(MediaType.parse("text/plain"), title1);
        complaint_department_id = RequestBody.create(MediaType.parse("text/plain"), departmentId1);
        complaint_user_id = RequestBody.create(MediaType.parse("text/plain"), userId1);
        complaint_details = RequestBody.create(MediaType.parse("text/plain"), details1);
        complaint_area_id= RequestBody.create(MediaType.parse("text/plain"), area_id);
        apiClient.createService(BASE_URL)
                .submitComplaint(complaint_title,complaint_department_id,body,complaint_user_id,complaint_details,complaint_file_name,complaint_area_id)
                .enqueue(new Callback<SubmitComplaintResponse>() {
                    @Override
                    public void onResponse(Call<SubmitComplaintResponse> call, Response<SubmitComplaintResponse> response) {
                        Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "ComplaintSubmitResponse: "+response.body());
                        mListener.onSuccessfulSubmit();
                    }

                    @Override
                    public void onFailure(Call<SubmitComplaintResponse> call, Throwable t) {
                        Log.i(TAG, "ComplaintSubmitFailure: "+t.getMessage());
                    }
                });

    }
    public boolean onValidate(String complaint_title, String complaint_detail) {

        if (TextUtils.isEmpty(complaint_title)) {
            mListener.showTitleBlankError();
            return false;
        } else if (TextUtils.isEmpty(complaint_detail)) {
            mListener.showDetailBlankError();
            return false;
        }

        return true;
    }


}
