package com.votermanagement.main.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.votermanagement.main.view.fragment.ComplaintListFragment;
import com.votermanagement.main.view.fragment.FacebookFeedListFragment;
import com.votermanagement.main.view.fragment.NotificationListFragment;
import com.votermanagement.main.view.fragment.TwitterFeedFragment;


public class CustomFragmentPageAdapter extends FragmentPagerAdapter {

    private static final String TAG = CustomFragmentPageAdapter.class.getSimpleName();

    private static final int FRAGMENT_COUNT = 4;

    public CustomFragmentPageAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ComplaintListFragment();
            case 1:
                return new NotificationListFragment();
            case 2:
                return new FacebookFeedListFragment();
            case 3:
                return new TwitterFeedFragment();
            /*case 1:
                return new PlaylistFragment();
            case 2:
                return new AlbumFragment();
            case 3:
                return new ArtistFragment();*/
        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "MY SECTION";
            case 1:
                return "UPDATES";
            case 2:
               // return "FACEBOOK";
            case 3:
                //return "TWITTER";
        }
        return null;
    }

}
