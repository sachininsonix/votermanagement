package com.votermanagement.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.votermanagement.main.R;
import com.votermanagement.main.customwidgets.RoundedImageView.RoundedImageView;
import com.votermanagement.main.model.facebook.DataItem;
import com.votermanagement.main.utils.Singleton;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.twitter.sdk.android.core.TwitterCore.TAG;

/**
 * Created by insonix on 20/9/17.
 */

public class FacebookFeedAdapter extends RecyclerView.Adapter<FacebookFeedAdapter.FacebookViewHolder> {
    private Context context;
    private List<DataItem> facebookData;

    public FacebookFeedAdapter(Context context, List<DataItem> facebookData) {
        this.context = context;
        this.facebookData = facebookData;

    }

    @Override
    public FacebookViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.facebook_feed_item, viewGroup, false);
        FacebookViewHolder facebookViewHolder = new FacebookViewHolder(view);
        return facebookViewHolder;
    }

    @Override
    public void onBindViewHolder(FacebookViewHolder viewHolder, int i) {
        DataItem dataItem = facebookData.get(i);
        viewHolder.bindFacbookData(dataItem);
    }

    @Override
    public int getItemCount() {

        return facebookData.size();

    }

    public class FacebookViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.facebook_profile_image)
        RoundedImageView facebook_profile_image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.timestamp)
        TextView timestamp;
        @BindView(R.id.txtStatusMsg)
        TextView txtStatusMsg;
        @BindView(R.id.feed_image)
        ImageView feedImage;
        @BindView(R.id.likes)
        TextView likes;
        @BindView(R.id.comments)
        TextView comments;

        public FacebookViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindFacbookData(DataItem dataItem) {
            Log.d(TAG, "bindFacbookData: " + dataItem.getLikes().getSummaryLikes().getTotalCount());

           /* name.setText(dataItem.getFrom().getName());
            timestamp.setText(dataItem.getCreatedTime());
            //likes.setText(dataItem.getLikes().getData().get);
            //comments.setText(dataItem.getComments().getSummary().getTotalCount());
            txtStatusMsg.setText(dataItem.getMessage());

            if(!TextUtils.isEmpty(dataItem.getPicture())){
                Picasso.with(context).load(dataItem.getPicture())
                        .into(feedImage);
            }else {
                feedImage.setVisibility(View.GONE);
            }

            String profileId = dataItem.getFrom().getId();
            URL img_value = null;
            try {
                img_value = new URL("https://graph.facebook.com/" + profileId + "/picture?type=large");
                Log.i(TAG, "getViewIma: " + img_value);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Picasso.with(context).load(String.valueOf(img_value)).into(facebook_profile_image);

*/
            name.setText(dataItem.getFrom().getName());
            //timestamp.setText(dataItem.getCreatedTime());
            timestamp.setText(Singleton.getInstance().formatFacebookFeedtime(dataItem.getCreatedTime()));

            if (dataItem.getMessage() == null) {
                txtStatusMsg.setVisibility(View.GONE);
            } else {
                txtStatusMsg.setText(dataItem.getMessage());
            }

            if (dataItem.getPicture() == null) {
                feedImage.setVisibility(View.GONE);
            } else {
                Picasso.with(context)
                        .load(dataItem.getPicture())
                        .into(feedImage);
            }
            String profileId = dataItem.getFrom().getId();
            URL img_value = null;
            try {
                img_value = new URL("https://graph.facebook.com/" + profileId + "/picture?type=large");
                Log.i(TAG, "getViewIma: " + img_value);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Picasso.with(context).load(String.valueOf(img_value)).into(facebook_profile_image);


            likes.setText(String.valueOf("likes: " + dataItem.getLikes().getSummaryLikes().getTotalCount()));
            comments.setText("comments: "+String.valueOf(dataItem.getComments().getSummary().getTotalCount()));




        }


    }


}
