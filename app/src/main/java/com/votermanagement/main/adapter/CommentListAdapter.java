package com.votermanagement.main.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.main.customwidgets.RoundedImageView.RoundedImageView;
import com.votermanagement.main.model.commentlist.CommentListData;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentListAdapter extends BaseAdapter implements Constants {


    private Context context;
    private ArrayList<CommentListData> commentData;

    public CommentListAdapter(Context context, ArrayList<CommentListData> commentData) {

        this.commentData = commentData;
        this.context = context;

    }


    @Override
    public int getCount() {
        return commentData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_layout, parent, false);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String reply_date1 = commentData.get(position).getReplyDate();
        String latest_status1 = commentData.get(position).getReplyStatus();
        holder.postedBy.setText(commentData.get(position).getUsername());
        holder.commentItem.setText(commentData.get(position).getMessage());
        holder.commentTime.setText(Singleton.getInstance().parseDateToddMMyyyy(context, reply_date1));
        holder.latestStatus.setText(Singleton.getInstance().getComplaintStatus(context, latest_status1));

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.profile_image)
        RoundedImageView profileImage;
        @BindView(R.id.posted_by)
        TextView postedBy;
        @BindView(R.id.comment_time)
        TextView commentTime;
        @BindView(R.id.comment_item)
        TextView commentItem;
        @BindView(R.id.latestStatus)
        TextView latestStatus;
        @BindView(R.id.commentlistloader)
        ProgressBar loader;
        @BindView(R.id.report_comment)
        TextView reportComment;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}