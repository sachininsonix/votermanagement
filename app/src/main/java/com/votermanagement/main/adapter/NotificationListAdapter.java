package com.votermanagement.main.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.votermanagement.main.R;
import com.votermanagement.main.customwidgets.RoundedImageView.RoundedImageView;
import com.votermanagement.main.model.notification.NotificationListData;
import com.votermanagement.main.view.fragment.NotificationDescriptionFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.twitter.sdk.android.core.TwitterCore.TAG;

/**
 * Created by insonix on 19/9/17.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    private List<NotificationListData> notificationListData;
    private FragmentActivity context;
    private ImageLoader imageLoader;
    DisplayImageOptions displayImageOptions;

    public NotificationListAdapter(FragmentActivity context, List<NotificationListData> notificationListData) {
        this.context = context;
        this.notificationListData = notificationListData;
        Log.i(TAG, "NotificationListAdapter: "+notificationListData);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        NotificationListData data = notificationListData.get(i);
        viewHolder.bindNotificationListData(data);

    }

    @Override
    public int getItemCount() {
        return notificationListData.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.notification_image)
        RoundedImageView notificationImage;
        @BindView(R.id.notification_title)
        TextView notificationTitle;
        @BindView(R.id.notification_desc)
        TextView notificationDesc;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bindNotificationListData(final NotificationListData data) {
            Log.i(TAG, "bindNotificationListData: "+data.getImage());
            if(!TextUtils.isEmpty(data.getImage())){
                Picasso.with(context)
                        .load(data.getImage())
                        .error(R.drawable.logo)
                        .placeholder(R.drawable.logo)
                        .into(notificationImage);
            }

           /* if (!TextUtils.isEmpty(data.getImage())) {
                imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(context));
                displayImageOptions= new DisplayImageOptions.Builder()
                        .showStubImage(R.drawable.logo)
                        .showImageForEmptyUri(R.drawable.logo)
                        .cacheOnDisc().cacheInMemory()
                        .postProcessor(new BitmapProcessor() {
                            @Override
                            public Bitmap process(Bitmap bmp) {
                                return Bitmap.createScaledBitmap(bmp, 60, 60, false);
                            }
                        }).build();
                imageLoader.displayImage(data.getImage(),notificationImage,displayImageOptions);
            }*/
            notificationDesc.setText(data.getMessage());
            notificationTitle.setText(data.getTitle());
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent intent=new Intent(context,NotificationDescriptionFragment.class);
                    intent.putExtra("notificationData",data);
                    context.startActivity(intent);*/
                    Fragment fragment=new NotificationDescriptionFragment();
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("notificationData",data);
                    fragment.setArguments(bundle);
                    context.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_container_wrapper, fragment)
                            .addToBackStack("notification list")
                            .commit();


                }
            });

        }
    }


}
