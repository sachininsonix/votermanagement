package com.votermanagement.main.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.votermanagement.main.R;
import com.votermanagement.main.customwidgets.RoundedImageView.RoundedImageView;
import com.votermanagement.main.model.complaintlist.ComplaintListData;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;
import com.votermanagement.main.view.fragment.ComplaintDescriptionFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

;

/**
 * Created by Sandeep on 9/16/2017.
 */

public class ComplaintListAdapter extends RecyclerView.Adapter<ComplaintListAdapter.ViewHolder> implements Constants {

    private FragmentActivity mContext;
//    private ArrayList<ComplaintListData> complaintList;
private List<ComplaintListData> complaintList;
    private ImageLoader imageLoader;
    DisplayImageOptions displayImageoptions;

    public ComplaintListAdapter(FragmentActivity mContext, List<ComplaintListData> complaintList) {
        this.mContext = mContext;
        this.complaintList = complaintList;
        Log.i(TAG, "ComplaintListAdapter: "+complaintList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.complaint_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ComplaintListData complaintListData = complaintList.get(position);
        holder.bindComplaintListData(complaintListData);

    }

    @Override
    public int getItemCount() {
        return complaintList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.complaint_image)
        RoundedImageView complaintImage;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.image_attachment)
        ImageView imageAttachment;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindComplaintListData(final ComplaintListData complaintListData) {
            if (!TextUtils.isEmpty(complaintListData.getAttachment())) {
               /* imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
                displayImageoptions= new DisplayImageOptions.Builder()
                        .showStubImage(R.drawable.logo)
                        .showImageForEmptyUri(R.drawable.logo)
                        .cacheOnDisc().cacheInMemory()
                        .postProcessor(new BitmapProcessor() {
                            @Override
                            public Bitmap process(Bitmap bmp) {
                                return Bitmap.createScaledBitmap(bmp, 60, 60, false);
                            }
                        }).build();
                imageLoader.displayImage(complaintListData.getAttachment(),complaintImage,displayImageoptions);*/
                Picasso.with(mContext)
                        .load(complaintListData.getAttachment())
                        .error(R.drawable.logo)
                        .placeholder(R.drawable.logo)
                        .into(complaintImage);
            }


            title.setText(complaintListData.getComplaintTitle());
            desc.setText(complaintListData.getDetails());
            String status1=complaintListData.getComplaintStatus();
            status.setText(Singleton.getInstance().getComplaintStatus(mContext,status1));
            String filenameArray[] = complaintListData.getAttachment().split("\\.");
            String extension = filenameArray[filenameArray.length - 1];
            Log.d(TAG, "bind: "+extension);
            if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg")) {
                imageAttachment.setVisibility(View.GONE);
            } else if (!TextUtils.isEmpty(extension) && !complaintListData.getAttachment().equals(BASE_URL+"images/")){
                imageAttachment.setVisibility(View.VISIBLE);
            }
            Log.d(TAG, "extension: " + complaintListData.getAttachment());
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new ComplaintDescriptionFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("complaintData", complaintListData);
                    fragment.setArguments(bundle);
                    mContext.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_container_wrapper, fragment)
                            .addToBackStack("complaintListFragment")
                            .commit();


                }
            });


        }
    }


}
