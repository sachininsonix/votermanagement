package com.votermanagement.main.view;


import com.votermanagement.main.model.deaprtment.DepartmentDataItem;

import java.util.List;

/**
 * Created by Sandeep on 9/17/2017.
 */

public interface DepartmentResponseView {
    void getDepartmentViews(List<DepartmentDataItem> data);
}
