package com.votermanagement.main.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.votermanagement.main.R;
import com.votermanagement.main.adapter.CommentListAdapter;
import com.votermanagement.main.adapter.Pdfview;
import com.votermanagement.main.customwidgets.FullLengthListView.FullLengthListView;
import com.votermanagement.main.interfaces.ChangeToogleButtonIconListener;
import com.votermanagement.main.interfaces.OnBackPressedListener;
import com.votermanagement.main.interfaces.ToolbatTitleChangeListener;
import com.votermanagement.main.model.commentlist.CommentListData;
import com.votermanagement.main.model.complaintlist.ComplaintListData;
import com.votermanagement.main.presenter.CommentListPresenter;
import com.votermanagement.main.presenter.CommentSubmitPresenter;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

/**
 * Created by insonix on 18/9/17.
 */

public class ComplaintDescriptionFragment extends Fragment implements Constants, CommentListPresenter.ReplyPresenterListener
        , CommentSubmitPresenter.CommentSubmitPresenterListener,OnBackPressedListener {


    @BindView(R.id.blur_image)
    ImageView blurImage;
    @BindView(R.id.complaint_image)
    ImageView complaintImage;
    @BindView(R.id.complaintDescriptionLoader_loader)
    ProgressBar complaintDescriptionLoader;
    @BindView(R.id.image_layout)
    RelativeLayout imageLayout;
    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.reportloader)
    ProgressBar reportloader;
    @BindView(R.id.report_complaint)
    TextView reportComplaint;
    @BindView(R.id.report_complaint_layout)
    LinearLayout reportComplaintLayout;
    @BindView(R.id.submittedBy)
    TextView submittedBy;
    @BindView(R.id.department)
    TextView department;
    @BindView(R.id.text_attachment)
    TextView textAttachment;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.commentLoader)
    ProgressBar commentlistloader;
    @BindView(R.id.comment_list)
    FullLengthListView commentList;
    @BindView(R.id.nocommentmment)
    TextView nocommentmment;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.view_lay)
    View viewLay;
    @BindView(R.id.comment)
    EditText comment;
    @BindView(R.id.spinner)
    MaterialBetterSpinner spinner;
    @BindView(R.id.btn_comment)
    Button btnComment;
    @BindView(R.id.lay1)
    LinearLayout lay1;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    Unbinder unbinder;
    private ComplaintListData userComplaintListData;
    private ImageLoader imagrLoader;
    private DisplayImageOptions displayImageOptions;
    private RenderScript renderScript;
    private CommentListAdapter commentListAdapter;

    private CommentListPresenter commentListPresenter;
    private CommentSubmitPresenter commentSubmitPresenter;
    private String status1;
    private String comment_status;
    ToolbatTitleChangeListener mListener;
    ChangeToogleButtonIconListener toogleButtonIconListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complaint_description_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        imagrLoader = ImageLoader.getInstance();
        imagrLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        displayImageOptions = new DisplayImageOptions.Builder().showStubImage(R.drawable.banner).showImageForEmptyUri(R.drawable.banner).cacheOnDisc().cacheInMemory().build();

        getIntentData();


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ToolbatTitleChangeListener) getActivity();
        toogleButtonIconListener = (ChangeToogleButtonIconListener) getActivity();

    }

    private void getIntentData() {
        userComplaintListData = (ComplaintListData) getArguments().getSerializable("complaintData");
        Log.d("ideaslistData12", String.valueOf(userComplaintListData));

        setComplaintData();
    }

    private void setComplaintData() {
        mListener.setToolbarTitle(userComplaintListData.getComplaintTitle());
        toogleButtonIconListener.showBackButton(true);
        commentListPresenter = new CommentListPresenter(getActivity(), this,commentlistloader);
        commentListPresenter.getAllComment(userComplaintListData.getComplaintId());


        details.setText(userComplaintListData.getDetails());
        submittedBy.setText(userComplaintListData.getUsername());
        department.setText(userComplaintListData.getDepartmentName());
        String submittedDate = userComplaintListData.getComplaintDate();
        date.setText(Singleton.getInstance().parseDateToddMMyyyy(getActivity(), submittedDate));
        status1 = userComplaintListData.getComplaintStatus();
        status.setText(Singleton.getInstance().getComplaintStatus(getActivity(), status1));
        spinner.setText(Singleton.getInstance().getComplaintStatus(getActivity(), status1));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_status, R.id.text, getResources().getStringArray(R.array.complaintStatus));
        spinner.setAdapter(adapter);

        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                comment_status = String.valueOf(adapterView.getAdapter().getItemId(i));
                Log.i(TAG, "CommentStatus: " + comment_status);
            }
        });


        String filenameArray[] = userComplaintListData.getAttachment().split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        Log.d(TAG, "setData: " + extension);
        Log.i(TAG, "setData: " + userComplaintListData.getAttachment());

        if (extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpeg")) {
            textAttachment.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(extension) && !userComplaintListData.getAttachment().equals(BASE_URL + "assets/flat_images/")) {
            textAttachment.setVisibility(View.VISIBLE);
            String splitted[] = userComplaintListData.getAttachment().split("assets/flat_images/");
            if (splitted.length > 0)
                textAttachment.setText(splitted[1]);
        } else {
            textAttachment.setVisibility(View.GONE);
        }
        if (textAttachment.getVisibility() == View.GONE && !TextUtils.isEmpty(userComplaintListData.getAttachment()) && !userComplaintListData.getAttachment().equals("http://khersocialapp.insonix.com/assets/flat_images/idea.png")) {
            imagrLoader.displayImage(userComplaintListData.getAttachment(), complaintImage, displayImageOptions);
            imagrLoader.loadImage(userComplaintListData.getAttachment(), new SimpleImageLoadingListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    Bitmap blurred = blurRenderScript(loadedImage, 25);//second parametre is radius
                    blurImage.setImageBitmap(blurred);
                }
            });
        } else {
            complaintImage.setImageResource(R.drawable.banner);
            // blurImage.setImageResource(R.drawable.banner);
        /*    Drawable drawable1=getResources().getDrawable(R.drawable.banner);
            BitmapDrawable drawable = (BitmapDrawable) drawable1;
            Bitmap bitmap = drawable.getBitmap();
            Bitmap blurred = blurRenderScript(bitmap, 25);*///second parametre is radius
            blurImage.setImageResource(R.drawable.banner);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        try {
            renderScript = RenderScript.create(getActivity());
            Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
            Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);
            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                    Element.U8_4(renderScript));
            blur.setInput(blurInput);
            blur.setRadius(radius); // radius must be 0 < r <= 25
            blur.forEach(blurOutput);

            blurOutput.copyTo(bitmap);
            renderScript.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;

    }


    private Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }


    @Override
    public void setCommentList(ArrayList<CommentListData> data) {
        Log.i(TAG, "CommentDataSize " + data.size());

        if (data.size() > 0) {
            commentList.setVisibility(View.VISIBLE);
            nocommentmment.setVisibility(View.GONE);
            commentListAdapter = new CommentListAdapter(getActivity(), data);
            commentList.setAdapter(commentListAdapter);
            //    commentList.setFocusableInTouchMode(true);
            commentListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onEmptyCommentList(String message) {
        commentList.setVisibility(View.GONE);
        nocommentmment.setVisibility(View.VISIBLE);
        nocommentmment.setText(message);
    }

    @Override
    public void onSuccessfulSubmit(String complaint_id) {
        commentListPresenter.getAllComment(complaint_id);
        if (!TextUtils.isEmpty(status1)) {

            status.setText(Singleton.getInstance().getComplaintStatus(getActivity(), comment_status));
        }

    }

    @Override
    public void showEmptyMessageError() {
        Toast.makeText(getActivity(), "Enter comment", Toast.LENGTH_SHORT).show();
        comment.requestFocus();


    }

    @OnClick({R.id.complaint_image, R.id.text_attachment, R.id.btn_comment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.complaint_image:
                if(userComplaintListData.getAttachment()!=null){
                    Singleton.getInstance().zoomDialog(getActivity(), null, userComplaintListData.getAttachment(), userComplaintListData.getComplaintTitle());

                }

                break;
            case R.id.text_attachment:

                Intent intent = new Intent(getActivity(), Pdfview.class);
                intent.putExtra("url", userComplaintListData.getAttachment().trim());
                startActivity(intent);
                break;
            case R.id.btn_comment:

                String complaint_id = userComplaintListData.getComplaintId();
                String user_id = Singleton.getInstance().getValue(getActivity(), USER_ID);
                String message = comment.getText().toString().trim();
                commentSubmitPresenter = new CommentSubmitPresenter(getActivity(), this);
                commentSubmitPresenter.submitComment(complaint_id, message, user_id, comment_status);
                break;
        }
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        toogleButtonIconListener.showBackButton(false);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_container_wrapper, new HomeFragment()).commit();
    }
}
