package com.votermanagement.main.view;


import com.votermanagement.main.model.AreaListModel.DataItem;

import java.util.List;

/**
 * Created by insonix on 29/8/17.
 */

public interface AreaResponseViews {

void getAreaViews(List<DataItem> data);

}
