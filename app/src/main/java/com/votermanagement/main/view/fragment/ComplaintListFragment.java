package com.votermanagement.main.view.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.main.adapter.ComplaintListAdapter;
import com.votermanagement.main.model.complaintlist.ComplaintListData;
import com.votermanagement.main.presenter.ComplaintListPresenterImpl;
import com.votermanagement.main.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ComplaintListFragment extends Fragment implements ComplaintListPresenterImpl.ComplaintListPresenterListener, Constants {


    @BindView(R.id.complaintlistloader)
    ProgressBar loader;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nocomment1)
    TextView nocomment1;
    Unbinder unbinder;
    ComplaintListPresenterImpl complaintListPresenter;
    @BindView(R.id.mRecycleComplaintList)
    RecyclerView mRecycleComplaintList;
    ComplaintListAdapter userComplaintListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.complaint_list_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        complaintListPresenter = new ComplaintListPresenterImpl(getActivity(), this,loader);
        complaintListPresenter.getcomplaintList();
        setRecyclerView(view);
        return view;
    }

    private void setRecyclerView(View view) {
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mRecycleComplaintList.setLayoutManager(mLayoutManager);
        // mRecycleComplaintList.addItemDecoration(new DividerItemDecoration(getActivity()));
        mRecycleComplaintList.setItemAnimator(new DefaultItemAnimator());
    }

  /*  @Override
    public void setComplaintList(ArrayList<ComplaintListData> complaintList) {
        if (loader.getVisibility() == View.VISIBLE) {
            loader.setVisibility(View.INVISIBLE);
        }
        Singleton.getInstance().complaintList = complaintList;

        nocomment1.setVisibility(View.GONE);
        mRecycleComplaintList.setVisibility(View.VISIBLE);
        userComplaintListAdapter = new ComplaintListAdapter(getActivity(), complaintList);
        mRecycleComplaintList.setAdapter(userComplaintListAdapter);
        userComplaintListAdapter.notifyDataSetChanged();


    }*/

    @Override
    public void setComplaintList(List<ComplaintListData> complaintList) {
        if (loader.getVisibility() == View.VISIBLE) {
            loader.setVisibility(View.INVISIBLE);
        }
        //Singleton.getInstance().complaintList = complaintList;

        nocomment1.setVisibility(View.GONE);
        mRecycleComplaintList.setVisibility(View.VISIBLE);
        userComplaintListAdapter = new ComplaintListAdapter(getActivity(), complaintList);
        mRecycleComplaintList.setAdapter(userComplaintListAdapter);
     //   userComplaintListAdapter.notifyDataSetChanged();


    }

    @Override
    public void emptyList(String msg) {
        if (loader.getVisibility() == View.VISIBLE) {
            loader.setVisibility(View.INVISIBLE);
        }
        mRecycleComplaintList.setVisibility(View.GONE);
        nocomment1.setVisibility(View.VISIBLE);
        nocomment1.setText(msg);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.fab_dialog);
        dialog.show();
        Button add_complained_btn;
        Button add_suggestion_btn;
        add_complained_btn = dialog.findViewById(R.id.dialog_btn_add_complained);
        add_suggestion_btn = dialog.findViewById(R.id.dialog_btn_add_suggestion);
        add_complained_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Bundle bundle=new Bundle();
                bundle.putInt("viewId",view.getId());
                ComplaintSubmitFragment complaintSubmitFragment=new ComplaintSubmitFragment();
                complaintSubmitFragment.setArguments(bundle);
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container_wrapper,complaintSubmitFragment).addToBackStack("");
                fragmentTransaction.commit();
            }
        });
        add_suggestion_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Bundle bundle=new Bundle();
                bundle.putInt("viewId",view.getId());
                ComplaintSubmitFragment complaintSubmitFragment=new ComplaintSubmitFragment();
                complaintSubmitFragment.setArguments(bundle);
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_container_wrapper,complaintSubmitFragment).addToBackStack("");
                fragmentTransaction.commit();
            }
        });
    }

}
