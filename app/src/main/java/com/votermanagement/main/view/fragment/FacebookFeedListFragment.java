package com.votermanagement.main.view.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.votermanagement.main.R;
import com.votermanagement.main.adapter.FacebookFeedAdapter;
import com.votermanagement.main.model.facebook.DataItem;
import com.votermanagement.main.presenter.FacebookFeedPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.twitter.sdk.android.core.TwitterCore.TAG;


/**
 * Created by Sandeep on 7/18/2017.
 */

public class FacebookFeedListFragment extends Fragment implements FacebookFeedPresenter.FacebookFeedPresenterListener {

    FacebookFeedPresenter facebookFeedPresenter;
    @BindView(R.id.facebookFeedLoader)
    ProgressBar loader;

    @BindView(R.id.mainLayout)
    CoordinatorLayout mainLayout;
    Unbinder unbinder;
    @BindView(R.id.mRecyclerFacebookFeedList)
    RecyclerView mRecyclerFacebookFeedList;
    private FacebookFeedAdapter facebookFeedAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.facebook_feed_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setRecyclerView(view);
        facebookFeedPresenter = new FacebookFeedPresenter(getActivity(), this, loader);
        facebookFeedPresenter.getAllFeed();

        return view;

    }

    private void setRecyclerView(View view) {
        if(mRecyclerFacebookFeedList!=null){
            GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);

            mRecyclerFacebookFeedList.setLayoutManager(mLayoutManager);
            // mRecycleComplaintList.addItemDecoration(new DividerItemDecoration(getActivity()));
            mRecyclerFacebookFeedList.setItemAnimator(new DefaultItemAnimator());
        }


    }

//    @Override
//    public void setFacebookFeed(ArrayList<DataItem> facebookDataArray) {
//        loader.setVisibility(View.GONE);
//        for(int i=0;i<facebookDataArray.size();i++){
//            Log.i(TAG, "setFacebookFeed: "+facebookDataArray.get(i).getMessage());
//        }
//        facebookFeedAdapter = new FacebookFeedAdapter(getActivity(), facebookDataArray);
//        mRecyclerFacebookFeedList.setAdapter(facebookFeedAdapter);
//        facebookFeedAdapter.notifyDataSetChanged();
//
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void setFacebookFeed(List<DataItem> facebookDataArray) {

        for (int i = 0; i < facebookDataArray.size(); i++) {
            Log.i(TAG, "setFacebookFeed111: " + facebookDataArray.get(i).getMessage());
        }
        facebookFeedAdapter = new FacebookFeedAdapter(getActivity(), facebookDataArray);
        if(mRecyclerFacebookFeedList!=null){
            mRecyclerFacebookFeedList.setAdapter(facebookFeedAdapter);
        }

     //   facebookFeedAdapter.notifyDataSetChanged();
    }
}
