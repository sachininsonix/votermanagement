package com.votermanagement.main.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.votermanagement.main.R;
import com.votermanagement.main.model.AreaListModel.DataItem;
import com.votermanagement.main.presenter.AreaPresenter;
import com.votermanagement.main.presenter.AreaPresenterImpl;
import com.votermanagement.main.presenter.SignupPresenter;
import com.votermanagement.main.utils.MySharedPreferences;
import com.votermanagement.main.view.AreaResponseViews;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;

import static com.votermanagement.main.R.id.signup_spinner;


/**
 * Created by insonix on 14/9/17.
 */

public class SignupActivity extends AppCompatActivity implements SignupPresenter.SignupPresenterListener, GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener, LocationListener, AreaResponseViews {
    private static final int ACCESS_FINE_LOCATION_CODE = 100;
    static final String TAG = SignupActivity.class.getSimpleName();
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 101;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar1)
    Toolbar toolbar1;
    @BindView(R.id.imageLogo)
    ImageView imageLogo;

    @BindView(R.id.signup_username)
    EditText signupUsername;
    @BindView(R.id.signup_password)
    EditText signupPassword;
    @BindView(R.id.signup_ph_no)
    EditText signupPhNo;
    @BindView(signup_spinner)
    AppCompatSpinner signupSpinner;
    @BindView(R.id.sign_up_btn)
    Button signUpBtn;
    SignupPresenter signupPresenter;
    MySharedPreferences mySharedPreference;
    @BindView(R.id.usernameLayout)
    LinearLayout usernameLayout;
    @BindView(R.id.passwordLayout)
    LinearLayout passwordLayout;
    @BindView(R.id.phoneNumberLayout)
    LinearLayout phoneNumberLayout;
    @BindView(R.id.areaLayout)
    LinearLayout areaLayout;
    MySharedPreferences sharedPreferences;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    HintSpinner<String> defaultHintSpinner;
    List<String> defaults;
    AreaPresenter areaPresenter;
    String area;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("SIGNUP");
        areaPresenter = new AreaPresenterImpl(this, SignupActivity.this);
        areaPresenter.getAreaList();
        checkLocationPermission();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_CODE);
        } else {

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    @OnClick(R.id.sign_up_btn)
    public void onViewClicked() {


        signupPresenter = new SignupPresenter(this, SignupActivity.this);
        String username = signupUsername.getText().toString().trim();

        String password = signupPassword.getText().toString().trim();

        String phone = signupPhNo.getText().toString().trim();
        String device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

       sharedPreferences=new MySharedPreferences(getApplicationContext());
        String device_token = sharedPreferences.getToken();

        String device_type = "android";
        String area_id = area;
        String latitude = String.valueOf(currentLatitude);

        String longitude = String.valueOf(currentLongitude);


        signupPresenter.signUp(username, password, phone, device_id, device_token, device_type, area_id, latitude, longitude);

    }

    @Override
    public void onSuccessfulRegistration() {
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showAreaError() {

    }

    @Override
    public void showPasswordError() {
        signupPassword.setError(getResources().getString(R.string.err_msg_password));
        requestFocus(signupPassword);
        signupPassword.requestFocus();
        passwordLayout.requestFocus();
    }

    @Override
    public void showUserNameError() {
        signupUsername.setError(getResources().getString(R.string.err_msg_username));
        requestFocus(signupUsername);
        signupUsername.requestFocus();
        usernameLayout.requestFocus();
    }

    @Override
    public void showPhoneNumberError() {
        signupPhNo.setError(getResources().getString(R.string.err_msg_phonenumber));
        requestFocus(signupPhNo);
        signupPhNo.requestFocus();
        phoneNumberLayout.requestFocus();
    }

    @Override
    public void showPhoneNumberLengthError() {
        signupPhNo.setError(getResources().getString(R.string.err_msg_phone_len));
        requestFocus(signupPhNo);
        signupPhNo.requestFocus();
        phoneNumberLayout.requestFocus();
    }

    @Override
    public void showPasswordLengthError() {
        signupPassword.setError(getResources().getString(R.string.err_msg_password_length));
        requestFocus(signupPassword);
        signupPassword.requestFocus();
        passwordLayout.requestFocus();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_CODE);
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            Log.i(TAG, "onConnected: " + currentLatitude);
            currentLongitude = location.getLongitude();
            Log.i(TAG, "onConnected: " + currentLongitude);


        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        Log.i(TAG, "onLocationChanged: " + currentLatitude);
        currentLongitude = location.getLongitude();
        Log.i(TAG, "onLocationChanged: " + currentLongitude);
    }

    private void initDefaultHintSpinner(final List<DataItem> data) {
        for (int i = 0; i < data.size(); i++) {
            defaults.add(data.get(i).getAreaName());

        }

        Log.i(TAG, "initDefaultHintSpinner: " + defaults.toString());
//
        defaultHintSpinner = new HintSpinner<>(signupSpinner, new HintAdapter<String>(this, R.string.default_spinner_hint, defaults),
                new HintSpinner.Callback<String>() {

                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {

                        showSelectedItem(itemAtPosition, data.get(position).getId());

                    }
                });


        defaultHintSpinner.init();
    }

    private void showSelectedItem(String itemAtPosition, String selected_area_id) {

        //area_selected= itemAtPosition;

        //Log.i(TAG, "showSelectedItem: "+area_selected_id);
        area = selected_area_id;
        Log.i(TAG, "showSelectedItem: "+area);


    }


    @Override
    public void getAreaViews(List<DataItem> data) {
        defaults = new ArrayList<>();

        initDefaultHintSpinner(data);
    }
}
