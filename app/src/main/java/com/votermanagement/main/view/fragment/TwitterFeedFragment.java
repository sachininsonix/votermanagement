package com.votermanagement.main.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;
import com.votermanagement.main.R;


/**
 * Created by Sandeep on 7/18/2017.
 */

public class TwitterFeedFragment extends ListFragment {
    ProgressBar loader;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=LayoutInflater.from(this.getActivity()).inflate(R.layout.twitter_feed_fragment,container,false);
        loader=view.findViewById(R.id.loadertwitter);

        TwitterConfig config = new TwitterConfig.Builder(getActivity())
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig("lg98LRj68kwYGL57hCu09rnVR", "LcCuLtM2HXNiYYBedUhiahN1yH4Xd0PKr5OycTmgxUkgQASf4L"))
                .debug(true)
                .build();
        Twitter.initialize(config);
        String username = "KirronKherBjp";
        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName(username)
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter(getActivity(), userTimeline);
        setListAdapter(adapter);
        loader.setVisibility(View.GONE);
        return view;
    }


}
