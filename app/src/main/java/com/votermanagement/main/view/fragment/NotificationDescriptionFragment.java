package com.votermanagement.main.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.votermanagement.main.R;
import com.votermanagement.main.interfaces.ChangeToogleButtonIconListener;
import com.votermanagement.main.interfaces.OnBackPressedListener;
import com.votermanagement.main.model.notification.NotificationListData;
import com.votermanagement.main.utils.Singleton;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 19/9/17.
 */

public class NotificationDescriptionFragment extends Fragment implements OnBackPressedListener {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.desc)
    TextView desc;
    @BindView(R.id.image)
    ImageView image;
    ChangeToogleButtonIconListener toogleButtonIconListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.notification_description_fragment,container,false);
        ButterKnife.bind(this,view);
        getIntentData();
        return view;
    }

    private void getIntentData() {
        NotificationListData data = (NotificationListData) getArguments().getSerializable("notificationData");
        setNotificationDescription(data);
    }



    private void setNotificationDescription(final NotificationListData data) {
        toogleButtonIconListener = (ChangeToogleButtonIconListener) getActivity();
        toogleButtonIconListener.showBackButton(true);
        title.setText(data.getTitle());
        desc.setText(data.getMessage());
        if(!TextUtils.isEmpty(data.getImage())){
            Picasso.with(getActivity())
                    .load(data.getImage())
//                    .error(R.drawable.logo)
//                    .placeholder(R.drawable.logo)
                    .into(image);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Singleton.getInstance().zoomDialog(getActivity(), null, data.getImage(),data.getTitle());
                }
            });
        }else {
            image.setVisibility(View.GONE);

        }


    }


    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
