package com.votermanagement.main.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.main.interfaces.ChangeToogleButtonIconListener;
import com.votermanagement.main.interfaces.OnBackPressedListener;
import com.votermanagement.main.interfaces.ToolbatTitleChangeListener;
import com.votermanagement.main.model.deaprtment.DepartmentDataItem;
import com.votermanagement.main.presenter.ComplaintSubmitPresenter;
import com.votermanagement.main.presenter.DepartmentPresenter;
import com.votermanagement.main.presenter.DepartmentPresenterImpl;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.Singleton;
import com.votermanagement.main.view.DepartmentResponseView;
import com.votermanagement.main.view.activity.HomeActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by Sandeep on 9/17/2017.
 */

public class ComplaintSubmitFragment extends Fragment implements DepartmentResponseView, Constants,
        ComplaintSubmitPresenter.ComplaintSubmitPresenterListener, OnBackPressedListener {
    private static final String TAG = ComplaintSubmitFragment.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int SELECT_PDF = 101;
    private static final int REQUEST_CAMERA = 102;
    private static final int SELECT_FILE = 103;

    DepartmentPresenter departmentPresenter;
    ArrayList<String> department;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.input_layout_name1)
    TextInputLayout inputLayoutName1;
    @BindView(R.id.spinnerComplaintAdd)
    MaterialBetterSpinner spinnerComplaintAdd;
    @BindView(R.id.details)
    EditText details;
    @BindView(R.id.ideaexplainlayout)
    TextInputLayout ideaexplainlayout;
    @BindView(R.id.text_doc)
    TextView textDoc;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.linear_upload)
    LinearLayout linearUpload;
    @BindView(R.id.submit)
    Button submit;
    Unbinder unbinder;
    String selectedDepartmentId;
    boolean result;
    private ComplaintSubmitPresenter complaintSubmitPresenter;
    private String mediaPath = "";
    private String userChoosenTask;
    private String imgPath;
    Bitmap thumbnail;
    private ChangeToogleButtonIconListener toogleButtonIconListener;
    private ToolbatTitleChangeListener toolbatTitleChangeListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complaint_add_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        toogleButtonIconListener = (ChangeToogleButtonIconListener) getActivity();
        toogleButtonIconListener.showBackButton(true);
        toolbatTitleChangeListener = (ToolbatTitleChangeListener) getActivity();
        int viewId = getArguments().getInt("viewId");
        if (viewId == R.id.dialog_btn_add_complained) {
            toolbatTitleChangeListener.setToolbarTitle("Add Complaint");
        } else {

            toolbatTitleChangeListener.setToolbarTitle("Add Suggestion");
        }
        departmentPresenter = new DepartmentPresenterImpl(getActivity(), this);
        departmentPresenter.getDepartmentList();


        return view;
    }

    public void getDepartmentViews(final List<DepartmentDataItem> data) {
        department = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            department.add(data.get(i).getDepartmentName());
        }
        //Log.i(TAG, "getDepartmentViews: "+department.size());

        ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), R.layout.spinner_department_item, department);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerComplaintAdd.setAdapter(adapter);

        spinnerComplaintAdd.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected: " + data.get(i).getId());
                selectedDepartmentId = data.get(i).getId();

            }
        });


    }


    @OnClick({R.id.linear_upload, R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_upload:

                if (Singleton.getInstance().checkPermission(getActivity())) {
                    result = true;
                    dialogSelect();

                }
                break;
            case R.id.submit:


                String title1 = title.getText().toString().trim();
                String departmentId1 = selectedDepartmentId;
                String details1 = details.getText().toString().trim();
                String area_id = Singleton.getInstance().getValue(getActivity(), AREA);
                String userId1 = Singleton.getInstance().getValue(getActivity(), USER_ID);
                complaintSubmitPresenter = new ComplaintSubmitPresenter(getActivity(), this);
                Log.i(TAG, "onViewClicked: " + complaintSubmitPresenter.onValidate(title1, details1));
                if (complaintSubmitPresenter.onValidate(title1, details1)) {

                    complaintSubmitPresenter.submitComplaint(title1, departmentId1, mediaPath, userId1, details1, area_id);
                    break;
                }
        }
    }

    private void dialogSelect() {
        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_share_doc, null);
        layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
        layout.setMinimumHeight((int) (displayRectangle.height() * 0.3f));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(layout);
        LinearLayout camera = (LinearLayout) dialog.findViewById(R.id.camera);
        LinearLayout gallery = (LinearLayout) dialog.findViewById(R.id.gallery);
        LinearLayout document = (LinearLayout) dialog.findViewById(R.id.document);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();

                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
                dialog.dismiss();
            }
        });
        document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userChoosenTask = "Document";
                if (result) {
                    docIntent();
                }
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void cameraIntent() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    private void galleryIntent() {


        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);

    }

    private void docIntent() {

        Intent intent = new Intent();
        intent.setType("application/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Select a File"), SELECT_PDF);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                mediaPath = getPath(getActivity(), data.getData());

                if (mediaPath != null) {
                    File file = new File(mediaPath);
                    textDoc.setText(file.getName());


                }
                Log.d(TAG, "onActivityResult: " + mediaPath);
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                Log.i(TAG, "onActivityResult:REQUEST_CAMERA " + getImagePath());
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                mediaPath = getPath(getActivity(), getImageUri(getActivity(), photo));
                Log.d(TAG, "onActivityResult: " + mediaPath);
                if (mediaPath != null) {
                    File file = new File(mediaPath);
                    textDoc.setText(file.getName());
                }


            } else if (requestCode == SELECT_PDF) {
                Uri uri = data.getData();
                Log.d(TAG, "onActivityResult: " + uri);
                if (uri != null) {
                    mediaPath = getPath(getActivity(), uri);
                    if (mediaPath != null) {
                        File file = new File(mediaPath);
                        textDoc.setText(file.getName());
                    } else {
                        alertDialog(getString(R.string.pdfNotAcces));
                    }

                } else {
                    mediaPath = "";

                }

                String displayName = null;
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getImagePath() {
        return imgPath;
    }

    void alertDialog(String msg) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle(getString(R.string.upload_file))
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }


    public static String getPath(final Context context, final Uri uri) {

        // check here to KITKAT or new version
        Log.d(TAG, "getPath: " + uri);
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap scaled = null;
        thumbnail = null;
        if (data != null) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                int nh = (int) (thumbnail.getHeight() * (512.0 / thumbnail.getWidth()));
                scaled = Bitmap.createScaledBitmap(thumbnail, 512, nh, true);
                String image1 = data.getData().toString();
                // compressImage(image1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // image.setImageBitmap(scaled);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else if (userChoosenTask.equals("Document")) {
                    Intent intent = new Intent();
                    intent.setType("application/*");
                    // intent.setType("*/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select a File"), SELECT_PDF);
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onSuccessfulSubmit() {
//        Intent intent = new Intent(this, HomeActivity.class);
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        //finish();

    }

  /*  @Override
    public void onBackPressed() {
//        Intent intent = new Intent(this, HomeActivity.class);
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
       // finish();
    }*/

    @Override
    public void showTitleBlankError() {
        title.setError(getResources().getString(R.string.err_complaint_title));
        title.requestFocus();

    }

    @Override
    public void showDetailBlankError() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        toogleButtonIconListener.showBackButton(false);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_container_wrapper, new HomeFragment()).commit();
    }

/*

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        null.unbind();
    }
*/


}
