package com.votermanagement.main.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.votermanagement.main.R;
import com.votermanagement.main.presenter.UserLoginPresenterImpl;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.MySharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by insonix on 15/9/17.
 */

public class LoginActivity extends AppCompatActivity implements Constants,UserLoginPresenterImpl.UserLoginPresenterListener{
    @BindView(R.id.imageLogo)
    ImageView imageLogo;
    @BindView(R.id.pr)
    ProgressBar pr;
    @BindView(R.id.login_username)
    EditText loginUsername;
    @BindView(R.id.usernameLayout)
    LinearLayout usernameLayout;
    @BindView(R.id.login_password)
    EditText loginPassword;
    @BindView(R.id.passwordLayout)
    LinearLayout passwordLayout;
    @BindView(R.id.text_fgt_pwd)
    TextView textFgtPwd;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.togo_signup_btn)
    Button togoSignupBtn;

    private MySharedPreferences mySharedPreference;

    private UserLoginPresenterImpl userLoginPresenterImpl;
    private static final String TAG=LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        userLoginPresenterImpl=new UserLoginPresenterImpl(this,this);
    }

    @OnClick({R.id.text_fgt_pwd, R.id.login_btn, R.id.togo_signup_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_fgt_pwd:
                Toast.makeText(this,"Forger password",Toast.LENGTH_SHORT).show();
                break;
            case R.id.login_btn:
                String username = loginUsername.getText().toString().trim();
                String password = loginPassword.getText().toString().trim();
                mySharedPreference = new MySharedPreferences(getApplicationContext());
                String device_token = mySharedPreference.getToken();
                Log.i(TAG, "onViewClicked: "+device_token);
                String device_type=DEVICE_TYPE;
                String device_id = Settings.Secure.getString(this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                userLoginPresenterImpl.login(username,password,device_id,device_token,device_type);

                break;
            case R.id.togo_signup_btn:
                Intent intent=new Intent(this,SignupActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onLoginSuccess() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showUserNameError() {
        loginUsername.setError(getResources().getString(R.string.err_msg_username));
        loginUsername.requestFocus();

    }

    @Override
    public void showPasswordError() {
        loginPassword.setError(getResources().getString(R.string.err_msg_password));
        loginPassword.requestFocus();

    }
}
