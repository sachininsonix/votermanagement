package com.votermanagement.main.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.votermanagement.main.R;
import com.votermanagement.main.adapter.NotificationListAdapter;
import com.votermanagement.main.model.notification.NotificationListData;
import com.votermanagement.main.presenter.NotificationListPresenter;
import com.votermanagement.main.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

;


public class NotificationListFragment extends Fragment implements Constants, NotificationListPresenter.NotificationListPresenterListener {

    @BindView(R.id.notificationlistloader)
    ProgressBar loader;
    @BindView(R.id.mRecyclerNotificationList)
    RecyclerView mRecyclerNotificationList;
    @BindView(R.id.nonotification)
    TextView nonotification;
    @BindView(R.id.mainLayout)
    CoordinatorLayout mainLayout;
    Unbinder unbinder;
    private NotificationListPresenter notificationListPresenter;
    private NotificationListAdapter notificationListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.notification_list_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setRecyclerView(view);
        notificationListPresenter = new NotificationListPresenter(getActivity(), this, loader);
        notificationListPresenter.getAllNotification();
        return view;
    }

    private void setRecyclerView(View view) {
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerNotificationList.setLayoutManager(mLayoutManager);
        // mRecycleComplaintList.addItemDecoration(new DividerItemDecoration(getActivity()));
        mRecyclerNotificationList.setItemAnimator(new DefaultItemAnimator());
    }

   /* @Override
    public void setNotificationList(ArrayList<NotificationListData> data) {
        mRecyclerNotificationList.setVisibility(View.VISIBLE);
        nonotification.setVisibility(View.GONE);
        notificationListAdapter = new NotificationListAdapter(getActivity(), data);
        mRecyclerNotificationList.setAdapter(notificationListAdapter);
        notificationListAdapter.notifyDataSetChanged();
        loader.setVisibility(View.GONE);

    }*/
    @Override
    public void setNotificationList(List<NotificationListData> data) {
        mRecyclerNotificationList.setVisibility(View.VISIBLE);
        nonotification.setVisibility(View.GONE);
        notificationListAdapter = new NotificationListAdapter(getActivity(), data);
        mRecyclerNotificationList.setAdapter(notificationListAdapter);
   //     notificationListAdapter.notifyDataSetChanged();
        loader.setVisibility(View.GONE);

    }
    @Override
    public void showEmptyNotificationList(String message) {
        loader.setVisibility(View.GONE);
        mRecyclerNotificationList.setVisibility(View.GONE);
        nonotification.setVisibility(View.VISIBLE);
        nonotification.setText(message);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
