package com.votermanagement.main.fcm;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.votermanagement.main.utils.Constants;
import com.votermanagement.main.utils.MySharedPreferences;


/**
 * Created by prashant on 17/10/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements Constants {

    private static final String TAG = "MyFirebaseIIDService";
    private String url;
    private String UId;
    private MySharedPreferences mySharedPreference;
    public static String refreshedToken;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        mySharedPreference = new MySharedPreferences(getApplicationContext());
        //Getting registration token
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        preferences= PreferenceManager.getDefaultSharedPreferences(MyFirebaseInstanceIDService.this);
        editor=preferences.edit();
        //Displaying token in logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        // sendRegistrationToServer(refreshedToken);
        mySharedPreference.saveNotificationSubscription(true,refreshedToken);
        editor.putString("on", "0");
        editor.commit();
    }

}




