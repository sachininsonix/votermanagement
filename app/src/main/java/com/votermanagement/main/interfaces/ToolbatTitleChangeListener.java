package com.votermanagement.main.interfaces;

/**
 * Created by insonix on 22/9/17.
 */

public interface ToolbatTitleChangeListener {
    void setToolbarTitle(String v);
}
