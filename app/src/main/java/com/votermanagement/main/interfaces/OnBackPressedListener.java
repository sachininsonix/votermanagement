package com.votermanagement.main.interfaces;

/**
 * Created by prashant on 15/11/16.
 */
public interface OnBackPressedListener {
    public void onBackPressed();
}
